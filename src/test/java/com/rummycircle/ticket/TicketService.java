package com.rummycircle.ticket;


import java.util.Arrays;
import java.util.List;


public class TicketService {
	
	private int id;
	private String name;
	private int ticketValue;
	private int ticketType;
	private int expiryType;
	private String expiryDate;
	private boolean displayToUsers;
	private int ticketStatus;
	private String createdBy;
	private String createdOn;
	private String updateDate;
	private String comments;
	private int errorcode;
	private List<ListDetails>details;
	private int noOfTickets;
	private String endDate;
	private long userIds[];
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getNoOfTickets() {
		return noOfTickets;
	}
	public void setNoOfTickets(int noOfTickets) {
		this.noOfTickets = noOfTickets;
	}
	public long[] getUserIds() {
		return userIds;
	}
	public void setUserIds(long[] userIds) {
		this.userIds = userIds;
	}
	
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTicketValue() {
		return ticketValue;
	}
	public void setTicketValue(int ticketValue) {
		this.ticketValue = ticketValue;
	}
	public int getTicketType() {
		return ticketType;
	}
	public void setTicketType(int ticketType) {
		this.ticketType = ticketType;
	}
	public int getExpiryType() {
		return expiryType;
	}
	public void setExpiryType(int expiryType) {
		this.expiryType = expiryType;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public boolean isDisplayToUsers() {
		return displayToUsers;
	}
	public void setDisplayToUsers(boolean displayToUsers) {
		this.displayToUsers = displayToUsers;
	}
	public int getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(int ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
	public List<ListDetails> getDetails() {
		return details;
	}
	public void setDetails(List<ListDetails> details) {
		this.details = details;
	}
	@Override
	public String toString() {
		return "SampleServiceClass [id=" + id + ", name=" + name
				+ ", ticketValue=" + ticketValue + ", ticketType=" + ticketType
				+ ", expiryType=" + expiryType + ", expiryDate=" + expiryDate
				+ ", displayToUsers=" + displayToUsers + ", ticketStatus="
				+ ticketStatus + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updateDate=" + updateDate + ", comments="
				+ comments + ", errorcode=" + errorcode + ", details="
				+ details + ", noOfTickets=" + noOfTickets + ", endDate="
				+ endDate + ", userIds=" + Arrays.toString(userIds)
				+ ", getEndDate()=" + getEndDate() + ", getNoOfTickets()="
				+ getNoOfTickets() + ", getUserIds()="
				+ Arrays.toString(getUserIds()) + ", getId()=" + getId()
				+ ", getName()=" + getName() + ", getTicketValue()="
				+ getTicketValue() + ", getTicketType()=" + getTicketType()
				+ ", getExpiryType()=" + getExpiryType() + ", getExpiryDate()="
				+ getExpiryDate() + ", isDisplayToUsers()="
				+ isDisplayToUsers() + ", getTicketStatus()="
				+ getTicketStatus() + ", getCreatedBy()=" + getCreatedBy()
				+ ", getCreatedOn()=" + getCreatedOn() + ", getUpdateDate()="
				+ getUpdateDate() + ", getComments()=" + getComments()
				+ ", getErrorcode()=" + getErrorcode() + ", getDetails()="
				+ getDetails() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	 
	
	
}








