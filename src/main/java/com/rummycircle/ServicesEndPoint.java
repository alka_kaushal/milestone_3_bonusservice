package com.rummycircle;

public interface ServicesEndPoint {
	
	// Endpoints for RPs 
	public static String JOIN_TOURNAMENT = "/nfs/api/rp/rct";
	public static String WITHDRAW_TOURNAMENT = "/nfs/api/rp/crpwt";
	public static String CREDIT_DEBIT_RP ="/nfs/api/nft/rp/creditdebit";
	public static String CREDIT_RP_POST_SETTLEMENT="/nfs/api/nft/rp/creditpostsettlement";
	public static String GET_RP_LP="/nfs/api/nft/nftdetails";
	public static String CREDIT_DEBIT_BULK_RP="/nfs/api/nft/rp/bulkCreditDebit";
	
	// Endpoints for Tickets
	public static String GET_TICKET_INFO_BY_ID="/nfs/api/ticket";
	public static String GET_TICKET_LIST="/nfs/api/ticket/";
	public static String CREATE_TICKET="/nfs/api/ticket/";
	public static String UPDATE_TICKET="/nfs/api/ticket";
	public static String ASSIGN_TO_USERS="/nfs/api/ticket/assignToUser";
	
	//Milestone 3 Tickets
	public static String USE_A_TICKET="/nfs/api/ticketUser/";
	public static String RESET_TICKET="/nfs/api/ticketUser/";
	public static String GET_USER_TICKET="/nfs/api/ticketUser/";

	//Milestone 2 Bonus Service @Author: Kshitija Sankhe 
	public static String USER_SPECIFIC_BONUS = "/bs/api/bonus/getUserSpecificBonus";
	public static String REWARD_STORE_BONUSES = "/bs/api/bonus/getBonusDetails";
	public static String ISSUE_BONUS = "/bs/api/bonus/issueBonus";
	
	//Milestone 3 Bonus Service @Author: Alka Kaushal
	public static String PROMO_CODES_LIST="/bs/api/bonus/getPromoCodes";
	public static String CREATE_PROMO_CODE="/bs/api/bonus/createPromoCode";
	public static String SEARCH_PROMO_CODE="/bs/api/bonus/searchPromoCode";
	public static String UPDATE_PROMO_CODE="/bs/api/bonus/updatePromoCode";
	public static String GET_BY_ORDERID="/bs/api/bonus/getByOrderId";
	
	

	

}
