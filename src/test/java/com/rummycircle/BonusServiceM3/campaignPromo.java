package com.rummycircle.BonusServiceM3;

public class campaignPromo {

	private String campaignName;
	private int offerType;
	private int status;

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public int getOfferType() {
		return offerType;
	}

	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "campaignPromo [campaignName=" + campaignName + ", offerType="
				+ offerType + ", status=" + status + "]";
	}

}
