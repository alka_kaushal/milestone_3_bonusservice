package com.rummycircle;


	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.ArrayList;
	import java.util.List;
	import java.util.Properties;

	import org.springframework.dao.EmptyResultDataAccessException;
	import org.springframework.jdbc.core.JdbcTemplate;
	import org.springframework.jdbc.core.RowMapper;
	import org.springframework.stereotype.Component;

	import com.rummycircle.utils.database.AbstractDAO;
	import com.rummycircle.utils.testutils.PropertyReader;

	@Component
	public class BonusDao extends AbstractDAO {
		Properties prop = PropertyReader.loadCustomProperties("custom.properties");
		private JdbcTemplate template;

		public BonusDao() {
			this.template = new JdbcTemplate(getDataSource());
		}

		public List<List<Object>> getUserSpecificBonus(long userid, long limit) {

			System.out.println("in DAO class");
			String query = "select bpu. bonus_promotion_id as promoId, bp.promo_type as promoType, bp.promo_code as promoCode, bp.bonus_type as bonusType, bpu.total_chunks as totalChunks,bpu.chunks_moved as chunksMoved,bpu.total_bonus_amount as totalBonusAmount,bpu.bonus_amount_perchunk as bonusAmountPerChunk,bpu.bonus_amount_released as bonusAmountReleased,bpu.total_entryfee as totalEntryFee,bpu.total_entryfee_played as totalEntryFeePlayed,bpu.entryfee_perchunk as entryFeePerChunk,bpu.bonus_start_date as bonusStartDate, bpu.bonus_end_date as bonusEndDate, bpu.bonus_expiry_date as bonusExpiryDate, bpu.status as status from bonus_promotions_users bpu, bonus_promotions bp where bp.id = bpu.bonus_promotion_id and bpu.user_id ="
					+ userid
					+ " and bpu.id >1 and bpu.status in ( 1,2 ) limit "
					+ limit + ";";

			try {

				return template.query(query, new RowMapper<List<Object>>() {
					@Override
					public List<Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						List<Object> list = new ArrayList<Object>();
						list.add(0, rs.getInt("promoId"));
						list.add(1, rs.getInt("promoType"));
						list.add(2, rs.getString("promoCode"));
						list.add(3, rs.getInt("bonusType"));
						list.add(4, rs.getInt("totalChunks"));
						list.add(5, rs.getInt("chunksMoved"));
						list.add(6, rs.getInt("totalBonusAmount"));
						list.add(7, rs.getInt("bonusAmountPerChunk"));
						list.add(8, rs.getInt("bonusAmountReleased"));
						list.add(9, rs.getInt("totalEntryFee"));
						list.add(10, rs.getInt("totalEntryFeePlayed"));
						list.add(11, rs.getInt("entryFeePerChunk"));
						list.add(12, rs.getString("bonusStartDate"));
						list.add(13, rs.getString("bonusEndDate"));
						list.add(14, rs.getString("bonusExpiryDate"));
						list.add(15, rs.getInt("status"));

						return list;
					}
				});
			} catch (EmptyResultDataAccessException e) {
				return null;
			}
		}

		public List<List<Object>> getRewardStoreBonuses(long promotype,
				String promoIds) {

			System.out.println("in DAO class method getRewardStoreBonuses");
			// String
			// query="select bpu. bonus_promotion_id as promoId, bp.promo_type as promoType, bp.promo_code as promoCode, bp.bonus_type as bonusType, bpu.total_chunks as totalChunks,bpu.chunks_moved as chunksMoved,bpu.total_bonus_amount as totalBonusAmount,bpu.bonus_amount_perchunk as bonusAmountPerChunk,bpu.bonus_amount_released as bonusAmountReleased,bpu.total_entryfee as totalEntryFee,bpu.total_entryfee_played as totalEntryFeePlayed,bpu.entryfee_perchunk as entryFeePerChunk,bpu.bonus_start_date as bonusStartDate, bpu.bonus_end_date as bonusEndDate, bpu.bonus_expiry_date as bonusExpiryDate, bpu.status as status from bonus_promotions_users bpu, bonus_promotions bp where bp.id = bpu.bonus_promotion_id and bpu.user_id ="+userid+" and bpu.id >1 and bpu.status in ( 1,2 ) limit "+limit+";";

			promoIds = promoIds.replace(":", ",");
			System.out.println("promo ids after replacing " + promoIds);

			String query = "select id,promo_type,promo_code,promo_start_date,promo_end_date,promo_usage,criteria_id,bonus_type,bonus_amount,min_deposit,percentage,max_bonus_amount,chunks,wager,bonus_expiry_days,promo_code_status from bonus_promotions where id in("
					+ promoIds + ")and promo_type=" + promotype + ";";
			System.out.println("=======query=========>>>" + query);
			try {

				return template.query(query, new RowMapper<List<Object>>() {
					@Override
					public List<Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						List<Object> list = new ArrayList<Object>();
						list.add(0, rs.getInt("id"));
						list.add(1, rs.getInt("promo_type"));
						list.add(2, rs.getString("promo_code"));
						list.add(3, rs.getString("promo_start_date"));
						list.add(4, rs.getString("promo_end_date"));
						list.add(5, rs.getInt("promo_usage"));
						list.add(6, rs.getInt("criteria_id"));
						list.add(7, rs.getInt("bonus_type"));
						list.add(8, rs.getInt("bonus_amount"));
						list.add(9, rs.getInt("min_deposit"));
						list.add(10, rs.getInt("percentage"));
						list.add(11, rs.getInt("max_bonus_amount"));
						list.add(12, rs.getInt("chunks"));
						list.add(13, rs.getString("wager"));
						list.add(14, rs.getString("bonus_expiry_days"));
						list.add(15, rs.getString("promo_code_status"));

						return list;
					}
				});
			} catch (EmptyResultDataAccessException e) {
				return null;
			}
		}

		public List<List<Object>> issueBonus(long userid, long promoid) {

			System.out.println("in DAO class method issueBonus");
			String query = "select * from bonus_promotions_users where user_id="
					+ userid + " and bonus_promotion_id= " + promoid + ";";
			try {

				return template.query(query, new RowMapper<List<Object>>() {
					@Override
					public List<Object> mapRow(ResultSet rs, int rowNum)
							throws SQLException {

						List<Object> list = new ArrayList<Object>();
						list.add(0, rs.getInt("id"));
						list.add(1, rs.getInt("bonus_promotion_id"));
						list.add(2, rs.getString("user_id"));
						list.add(3, rs.getString("order_id"));
						list.add(4, rs.getString("is_payment_authorised"));
						list.add(5, rs.getInt("total_chunks"));
						list.add(6, rs.getInt("chunks_moved"));
						list.add(7, rs.getInt("total_bonus_amount"));
						list.add(8, rs.getInt("bonus_amount_perchunk"));
						list.add(9, rs.getInt("bonus_amount_released"));
						list.add(10, rs.getInt("total_entryfee"));
						list.add(11, rs.getInt("total_entryfee_played"));
						list.add(12, rs.getInt("entryfee_perchunk"));
						list.add(13, rs.getString("bonus_start_date"));
						list.add(14, rs.getString("bonus_end_date"));
						list.add(15, rs.getString("bonus_expiry_date"));
						list.add(16, rs.getString("status"));
						list.add(17, rs.getString("issued_by"));
						list.add(18, rs.getString("update_date"));

						return list;
					}
				});
			} catch (EmptyResultDataAccessException e) {
				return null;
			}
		}

		public int getCount() {
			String query = String
					.format("select count(id) from ticket_creation_details;");

			Integer result = null;

			try {

				result = template.queryForObject(query, Integer.class);
				return result;

			} catch (EmptyResultDataAccessException e) {
				return 0;
			}

		}

	}// end of DAO class


