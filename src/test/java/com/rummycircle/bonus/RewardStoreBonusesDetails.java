package com.rummycircle.bonus;

public class RewardStoreBonusesDetails {
	
	
	 private int id;
	    private int promotionType;
		private String promotionCode;
	    private String promoStartDate;
		private String promoEndDate;
		private int promoUsage;
		private int criteriaId;
		private int bonusType;
		private int bonusAmount;
		private int minDeposit;
		private int percentage;
		private int maxBonusAmount;
		private int chunks;
		private int wager;
		private int bonusExpiryDays;
		private int promoCodeStatus;
	    
	    
	    public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getPromotionType() {
			return promotionType;
		}
		public void setPromotionType(int promotionType) {
			this.promotionType = promotionType;
		}
		public String getPromotionCode() {
			return promotionCode;
		}
		public void setPromotionCode(String promotionCode) {
			this.promotionCode = promotionCode;
		}
		public String getPromoStartDate() {
			return promoStartDate;
		}
		public void setPromoStartDate(String promoStartDate) {
			this.promoStartDate = promoStartDate;
		}
		public String getPromoEndDate() {
			return promoEndDate;
		}
		public void setPromoEndDate(String promoEndDate) {
			this.promoEndDate = promoEndDate;
		}
		public int getPromoUsage() {
			return promoUsage;
		}
		public void setPromoUsage(int promoUsage) {
			this.promoUsage = promoUsage;
		}
		public int getCriteriaId() {
			return criteriaId;
		}
		public void setCriteriaId(int criteriaId) {
			this.criteriaId = criteriaId;
		}
		public int getBonusType() {
			return bonusType;
		}
		public void setBonusType(int bonusType) {
			this.bonusType = bonusType;
		}
		public int getBonusAmount() {
			return bonusAmount;
		}
		public void setBonusAmount(int bonusAmount) {
			this.bonusAmount = bonusAmount;
		}
		public int getMinDeposit() {
			return minDeposit;
		}
		public void setMinDeposit(int minDeposit) {
			this.minDeposit = minDeposit;
		}
		public int getPercentage() {
			return percentage;
		}
		public void setPercentage(int percentage) {
			this.percentage = percentage;
		}
		public int getMaxBonusAmount() {
			return maxBonusAmount;
		}
		public void setMaxBonusAmount(int maxBonusAmount) {
			this.maxBonusAmount = maxBonusAmount;
		}
		public int getChunks() {
			return chunks;
		}
		public void setChunks(int chunks) {
			this.chunks = chunks;
		}
		public int getWager() {
			return wager;
		}
		public void setWager(int wager) {
			this.wager = wager;
		}
		public int getBonusExpiryDays() {
			return bonusExpiryDays;
		}
		public void setBonusExpiryDays(int bonusExpiryDays) {
			this.bonusExpiryDays = bonusExpiryDays;
		}
		public int getPromoCodeStatus() {
			return promoCodeStatus;
		}
		public void setPromoCodeStatus(int promoCodeStatus) {
			this.promoCodeStatus = promoCodeStatus;
		}
		
		
	   

}
