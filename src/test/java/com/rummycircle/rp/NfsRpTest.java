package com.rummycircle.rp;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.rummycircle.NfsDao;
import com.rummycircle.ServicesEndPoint;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycircle.utils.testutils.PropertyReader;

public class NfsRpTest extends BaseTest {

	static NfsDao dbObj = new NfsDao();

	private static Logger Log = Logger.getLogger(NfsRpTest.class);
	static Properties prop = PropertyReader
			.loadCustomProperties("custom.properties");

	// Tests for NFS RPs

	// @Test
	public void JOIN_TOURNAMENT() {

		String path = ServicesEndPoint.JOIN_TOURNAMENT;

		RPCreditServiceClass serviceObj = new RPCreditServiceClass();
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTournamentId(Long.parseLong(prop.getProperty(
				"tournamentId").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty(
				"rewardPointValue").trim()));

		int initialgetRPfromNFT = dbObj.getRPfromNFT(Long.parseLong(prop
				.getProperty("userId").trim()));
		int initialgetRPfromMonthly_EntryFee = dbObj
				.getRPfromMonthly_EntryFee(Long.parseLong(prop.getProperty(
						"userId").trim()));

		HTTPRequest request = new HTTPRequest(serviceObj);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);
		Log.info("response body:" + httpResp.getBody().getBodyText());
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());

		int getRPfromNFT = dbObj.getRPfromNFT(Long.parseLong(prop.getProperty(
				"userId").trim()));
		int getRPfromMonthly_EntryFee = dbObj.getRPfromMonthly_EntryFee(Long
				.parseLong(prop.getProperty("userId").trim()));

		Assert.assertEquals((initialgetRPfromNFT - getRPfromNFT),
				serviceObj.getRewardPointValue());
		Assert.assertEquals(
				(initialgetRPfromMonthly_EntryFee - getRPfromMonthly_EntryFee),
				serviceObj.getRewardPointValue());

	}

	// @Test
	public void WITHDRAW_TOURNAMENT() {

		String path = ServicesEndPoint.WITHDRAW_TOURNAMENT;

		RPCreditServiceClass serviceObj = new RPCreditServiceClass();
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTournamentId(Long.parseLong(prop.getProperty(
				"tournamentId").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty(
				"rewardPointValue").trim()));

		int initialresultDB = dbObj.getRPfromNFT(Long.parseLong(prop
				.getProperty("userId").trim()));

		HTTPRequest request = new HTTPRequest(serviceObj);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

		int resultDB = dbObj.getRPfromNFT(Long.parseLong(prop.getProperty(
				"userId").trim()));
		Assert.assertEquals((resultDB - initialresultDB),
				serviceObj.getRewardPointValue());

		Log.info("response body: " + httpResp.getBody().getBodyText());
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());

	}

	/*
	 * CREDIT_DEBIT_BULK_RP //@Test public void
	 * CREDIT_RP_POSTSETTLEMENT(){if(Integer.parseInt(prop.getProperty
	 * ("CREDIT_DEBIT").trim())==1){
	 * 
	 * Assert.assertEquals((resultDB-initialresultDB),serviceObj.getRewardPointValue
	 * () );
	 * Assert.assertEquals((finalGetPlayerRp-initialGetPlayerRp),serviceObj
	 * .getRewardPointValue());
	 * 
	 * } else{
	 * 
	 * Assert.assertEquals((initialresultDB-resultDB),serviceObj.getRewardPointValue
	 * () );
	 * Assert.assertEquals((initialGetPlayerRp-finalGetPlayerRp),serviceObj
	 * .getRewardPointValue());
	 * 
	 * }
	 * 
	 * 
	 * String path = ServicesEndPoint.CREDIT_RP_POST_SETTLEMENT;
	 * 
	 * RPCreditServiceClass serviceObj = new RPCreditServiceClass();
	 * 
	 * //Check initial value from the database int initialresultDB =
	 * dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
	 * 
	 * serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
	 * serviceObj
	 * .setTournamentId(Long.parseLong(prop.getProperty("tournamentId")
	 * .trim()));
	 * serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty(
	 * "rewardPointValue").trim()));
	 * serviceObj.setNewrpSpendCounter(Double.parseDouble
	 * (prop.getProperty("newrpSpendCounter").trim()));
	 * 
	 * 
	 * 
	 * HTTPRequest request = new HTTPRequest(serviceObj);
	 * 
	 * HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);
	 * 
	 * 
	 * // Get final values from database after update int resultDB=
	 * dbObj.getRPfromNFT(Long.parseLong(prop.getProperty("userId").trim()));
	 * 
	 * // Check if the initial values and the final values are correct
	 * Assert.assertEquals
	 * ((resultDB-initialresultDB),serviceObj.getRewardPointValue() );
	 * 
	 * Log.info("response body:" + httpResp.getBody().getBodyText());
	 * Log.info("status code :" + httpResp.getStatusCode());
	 * Log.info("status code :" + httpResp.getReasonPhrase());
	 * 
	 * 
	 * }
	 */

	//@Test
	public void CREDIT_DEBIT_RP() {

		String path = ServicesEndPoint.CREDIT_DEBIT_RP;

		CreditOnVerificationServiceClass serviceObj = new CreditOnVerificationServiceClass();

		// Check initial value from the database

		int initialRPfromNFT = dbObj.getRPfromNFT(Long.parseLong(prop
				.getProperty("userId").trim()));
		int initialGetPlayerRp = dbObj.getRPfromPlayer_RP(Long.parseLong(prop
				.getProperty("userId").trim()));
		int initialRPfromMonthlyEntry = dbObj.getRPfromMonthly_EntryFee(Long
				.parseLong(prop.getProperty("userId").trim()));

		// setting the values to be posted in the url
		serviceObj.setUserId(Long.parseLong(prop.getProperty("userId").trim()));
		serviceObj.setTournamentId(Integer.parseInt(prop.getProperty(
				"tournamentId").trim()));
		serviceObj.setSysAccId(Integer.parseInt(prop.getProperty("sysAccId")
				.trim()));
		serviceObj.settxnID(Long.parseLong(prop.getProperty("txnID").trim()));
		serviceObj.setRewardPointValue(Long.parseLong(prop.getProperty(
				"rewardPointValue").trim()));
		serviceObj.setRewardStoreItemId(Long.parseLong(prop.getProperty(
				"rewardStoreItemId").trim()));
		serviceObj.setRewardStorePrice(Double.parseDouble(prop.getProperty(
				"rewardStorePrice").trim()));
		serviceObj.setNewrpSpendCounter(Double.parseDouble(prop.getProperty(
				"newrpSpendCounter").trim()));
		serviceObj.setRupeeValue(Double.parseDouble(prop.getProperty(
				"rupeeValue").trim()));
		serviceObj.setComment(prop.getProperty("comment").trim());
		serviceObj.setCreditDebit(Integer.parseInt(prop.getProperty(
				"creditDebit").trim()));

		HTTPRequest request = new HTTPRequest(serviceObj);
		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

		System.out.println("serviceObj:" + serviceObj);

		// Get final values from database after update
		int finalRPfromNFT = dbObj.getRPfromNFT(Long.parseLong(prop
				.getProperty("userId").trim()));
		int finalGetPlayerRp = dbObj.getRPfromPlayer_RP(Long.parseLong(prop
				.getProperty("userId").trim()));
		int finalRPfromMonthlyEntry = dbObj.getRPfromMonthly_EntryFee(Long
				.parseLong(prop.getProperty("userId").trim()));

		// Check if the initial values and the final values are correct
		if (Integer.parseInt(prop.getProperty("creditDebit").trim()) == 1) {

			Assert.assertEquals((finalRPfromNFT - initialRPfromNFT),
					serviceObj.getRewardPointValue());
			Assert.assertEquals((finalGetPlayerRp - initialGetPlayerRp),
					serviceObj.getRewardPointValue());
			Assert.assertEquals(
					(finalRPfromMonthlyEntry - initialRPfromMonthlyEntry),
					serviceObj.getRewardPointValue());

		} else {

			Assert.assertEquals((initialRPfromNFT - finalRPfromNFT),
					serviceObj.getRewardPointValue());
			Assert.assertEquals((initialGetPlayerRp - finalGetPlayerRp),
					serviceObj.getRewardPointValue());
			Assert.assertEquals((initialGetPlayerRp - finalRPfromMonthlyEntry),
					serviceObj.getRewardPointValue());
		}

		Log.info("Assertions completed succesfully");

		Log.info("response body: " + httpResp.getBody().getBodyText());
		Log.info("status code :" + httpResp.getStatusCode());
		Log.info("status code :" + httpResp.getReasonPhrase());
	}

	
	@Test
	public void GET_RP_LP() {

		String useridString = prop.getProperty("userId");
		Long userid = Long.parseLong(useridString);

		String path = ServicesEndPoint.GET_RP_LP + "/" + userid;

		List<List<Object>> ListFromDB = dbObj.getUserDataFromNFT(userid);

		int db_monthly_lp = (int) ListFromDB.get(0).get(0);
		int db_vip_lp = (int) ListFromDB.get(0).get(1);
		int db_rp_balance = (int) ListFromDB.get(0).get(2);
		int db_club_type = (int) ListFromDB.get(0).get(3);

		List<List<Object>> ListforCLUB = dbObj.getCLUB_LP(db_club_type + 1);
		// String club_name = (String) ListforCLUB.get(0).get(0);
		int min_lp_required = (int) ListforCLUB.get(0).get(1);

		Object httpReqObj = null;

		HTTPRequest httpRequest = new HTTPRequest(httpReqObj);
		HTTPResponse HTTPresponse = rc.sendRequest(HTTPMethod.GET, path,
				httpRequest);

		System.out.println("Response Body: "
				+ HTTPresponse.getBody().getBodyText());

		RP_LPServiceClass responseBody = deserializeJsonToJavaObject(
				HTTPresponse.getBody().getBodyText(), RP_LPServiceClass.class);

		Assert.assertEquals(HTTPresponse.getStatusCode(), 200);
		Assert.assertEquals(db_monthly_lp, responseBody.getMonthly_lp());
		Assert.assertEquals(db_vip_lp, responseBody.getVip_lp());
		Assert.assertEquals(db_rp_balance, responseBody.getRp_balance());
		Assert.assertEquals(db_club_type,
				Integer.parseInt(responseBody.getClubType()));
		// Assert.assertEquals(club_name, responseBody.getNextClubType());
		Assert.assertEquals(min_lp_required, responseBody.getLpForNextClub());

	}

	//@Test
	public static void CREDIT_DEBIT_BULK_RP() {

		String path = ServicesEndPoint.CREDIT_DEBIT_BULK_RP;

		JSONObject mainObj = new JSONObject();
		ArrayList<JSONObject> jsonArray = new<JSONObject> ArrayList();

		//For production testing
		 /* List<List<Object>> listUser = dbObj.getInfoForBulkRPCredit();
		  
		  for(int i=0; i<listUser.size();i++) { 
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("user_id", listUser.get(i).get(0));
			jsonObj.put("rp", listUser.get(i).get(1));
			// System.out.println("JsonObject : "+jsonObj);
			jsonArray.add(jsonObj); 
		  }*/
		 

		for (int i = 0; i < Long.parseLong(prop
				.getProperty("NoOfObjForBulkRps").trim()); i++) {

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("user_id",
					Long.parseLong(prop.getProperty("userId").trim()) + i);
			jsonObj.put("rp",
					Long.parseLong(prop.getProperty("rewardPointValue").trim())
							+ i);
			jsonArray.add(jsonObj);

		}

		mainObj.put("userDetail", jsonArray);
		mainObj.put("rewardStoreItemId",
				Integer.parseInt(prop.getProperty("rewardStoreItemId").trim()));
		mainObj.put("rewardStorePrice",
				Double.parseDouble(prop.getProperty("rewardStorePrice").trim()));
		/*mainObj.put("rupeeValue",Double.parseDouble(prop.getProperty("rupeeValue").trim()));*/
		mainObj.put("comment", prop.getProperty("comment"));
		mainObj.put("sysAccId",
				Integer.parseInt(prop.getProperty("sysAccId").trim()));
		mainObj.put("txnID", Integer.parseInt(prop.getProperty("txnID").trim()));
		mainObj.put("creditDebit",
				Integer.parseInt(prop.getProperty("creditDebit").trim()));

		HTTPRequest request = new HTTPRequest(mainObj);
		System.out.println(mainObj);
		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.PUT, path, request);

		System.out.println("Response Body: " + httpResp.getBody().getBodyText());
		System.out.println("Status Code: " + httpResp.getStatusCode());

	}

	//@AfterMethod
	public void getRunTime(ITestResult tr) {
		long time = tr.getEndMillis() - tr.getStartMillis();
		System.out.println("Time taken for the test to run in Milliseconds :"
				+ time);
	}

}
