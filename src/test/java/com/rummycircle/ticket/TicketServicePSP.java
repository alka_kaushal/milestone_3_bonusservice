package com.rummycircle.ticket;

public class TicketServicePSP {
	
	
	private int userID;
	private int ticketID;
	private int tournamentID;
	
	@Override
	public String toString() {
		return "SampleServiceClass [userID=" + userID + ", ticketID="
				+ ticketID + ", tournamentID=" + tournamentID + ", id=" + id
				+ "]";
	}
	

	
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public int getTicketID() {
		return ticketID;
	}
	public void setTicketID(int ticketID) {
		this.ticketID = ticketID;
	}
	public int getTournamentID() {
		return tournamentID;
	}
	public void setTournamentID(int tournamentID) {
		this.tournamentID = tournamentID;
	}
	
	

}



