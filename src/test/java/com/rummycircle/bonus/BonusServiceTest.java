package com.rummycircle.bonus;


	import java.util.HashMap;
	import java.util.List;
	import java.util.Properties;

	import org.apache.log4j.Logger;
	import org.testng.annotations.Test;

	import com.jayway.jsonpath.JsonPath;
	import com.rummycircle.BonusDao;
	import com.rummycircle.ServicesEndPoint;
	import com.rummycircle.restclient.HTTPMethod;
	import com.rummycircle.restclient.HTTPRequest;
	import com.rummycircle.restclient.HTTPResponse;
	import com.rummycircle.utils.exceptions.RCException;
	import com.rummycircle.utils.testutils.BaseTest;
	import com.rummycircle.utils.testutils.PropertyReader;

	/**
	 * @author kshitija
	 *
	 */
	public class BonusServiceTest extends BaseTest {

		private static Logger Log = Logger.getLogger(BonusServiceTest.class);
		Properties prop = PropertyReader.loadCustomProperties("custom.properties");

		private BonusDao bonusdao;

		public BonusServiceTest() {
			bonusdao = new BonusDao();
		}
		
		

		/**
		 GET method: returns userspecific bonuses based on userid and status
		 */

		@Test
		public void getUserSpecificBonus() {

			String uid = prop.getProperty("USERID");
			long userid = Long.parseLong(uid);

			String lid = prop.getProperty("LASTID");
			long lastid = Long.parseLong(lid);

			String lim = prop.getProperty("LIMIT");
			long limit = Long.parseLong(lim);

			String status = prop.getProperty("STATUS");
			
			String path = ServicesEndPoint.USER_SPECIFIC_BONUS + "?userId="
					+ userid + "&lastId=" + lastid + "&limit=" + limit + "&status="
					+ status;
			System.out.println("path is " + path);

			BonusService service = new BonusService();

			Object str = null;
			HTTPRequest request = new HTTPRequest(str);
			System.out.println("request is  " + request.toString());

			HTTPResponse response = rc.sendRequest(HTTPMethod.GET, path, request);
			System.out
					.println("RESPONSE BODYYY" + response.getBody().getBodyText());
			System.out.println("RESPONSE STATUS" + response.getStatusCode());

			List<HashMap> details = JsonPath
					.parse(response.getBody().getBodyText()).read("$.[*]");

			List<List<Object>> items = bonusdao.getUserSpecificBonus(userid, limit);

			try {

				for (int j = 0; j < details.size(); j++) {
					HashMap map = details.get(j);

					System.out.println("promo id: " + items.get(j).get(0) + " ** "
							+ map.get("promoId"));
					System.out.println("promoType: " + items.get(j).get(1) + " ** "
							+ map.get("promoType"));
					System.out.println("promoCode: " + items.get(j).get(2) + " ** "
							+ map.get("promoCode"));
					System.out.println("bonusType: " + items.get(j).get(3) + " ** "
							+ map.get("bonusType"));
					System.out.println("totalChunks: " + items.get(j).get(4)
							+ " ** " + map.get("totalChunks"));
					System.out.println("chunksMoved: " + items.get(j).get(5)
							+ " ** " + map.get("chunksMoved"));
					System.out.println("totalBonusAmount: " + items.get(j).get(6)
							+ " ** " + map.get("totalBonusAmount"));
					System.out.println("bonusAmountPerChunk: "
							+ items.get(j).get(7) + " ** "
							+ map.get("bonusAmountPerChunk"));
					System.out.println("bonusAmountReleased: "
							+ items.get(j).get(8) + " ** "
							+ map.get("bonusAmountReleased"));
					System.out.println("totalEntryFee: " + items.get(j).get(9)
							+ " ** " + map.get("totalEntryFee"));
					System.out.println("totalEntryFeePlayed: "
							+ items.get(j).get(10) + " ** "
							+ map.get("totalEntryFeePlayed"));
					System.out.println("entryFeePerChunk: " + items.get(j).get(11)
							+ " ** " + map.get("entryFeePerChunk"));
					System.out.println("bonusStartDate: " + items.get(j).get(12)
							+ " ** " + map.get("bonusStartDate"));
					System.out.println("bonusEndDate: " + items.get(j).get(13)
							+ " ** " + map.get("bonusEndDate"));
					System.out.println("bonusExpiryDate: " + items.get(j).get(14)
							+ " ** " + map.get("bonusExpiryDate"));
					System.out.println("status: " + items.get(j).get(15) + " ** "
							+ map.get("status"));
				}

			}

			catch (RCException e) {
				System.out.println("in exception");
				Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
			}

		}// eof ticketlist

		// GET: getUserSpecificBonus
		@Test
		public void getBonusDetails() {

			String proType = prop.getProperty("PROMOTYPE");
			long promoType = Long.parseLong(proType);

			String lid = prop.getProperty("LASTID");
			long lastid = Long.parseLong(lid);

			String lim = prop.getProperty("LIMIT");
			long limit = Long.parseLong(lim);

			String promoIds = prop.getProperty("PROMOIDS");
			

			String path = ServicesEndPoint.REWARD_STORE_BONUSES + "?promoType="
					+ promoType + "&promoIds=" + promoIds + "&limit=" + limit
					+ "&lastId=" + lastid;
			System.out.println(" ** path is ** " + path);

			BonusService service = new BonusService();

			Object str = null;
			HTTPRequest request = new HTTPRequest(str);
			System.out.println("request is  " + request.toString());

			HTTPResponse response = rc.sendRequest(HTTPMethod.GET, path, request);
			System.out.println("RESPONSE BODYYY" + response.getBody().getBodyText());
			System.out.println("RESPONSE STATUS" + response.getStatusCode());

			List<HashMap> details = JsonPath
					.parse(response.getBody().getBodyText()).read("$.[*]");

			List<List<Object>> items = bonusdao.getRewardStoreBonuses(promoType,
					promoIds);
			try {

				for (int j = 0; j < details.size(); j++) {

					System.out.println("details size ===========" + details.size());
					HashMap map = details.get(j);

					System.out.println("promo id db: " + items.get(j).get(0)
							+ " ** " + map.get("id"));
					System.out.println("promoType db: " + items.get(j).get(1)
							+ " ** " + map.get("promotionType"));
					System.out.println("promoCode db: " + items.get(j).get(2)
							+ " ** " + map.get("promotionCode"));
					System.out.println("promoStartDate db: " + items.get(j).get(3)
							+ " ** " + map.get("promoStartDate"));
					System.out.println("promoEndDate db: " + items.get(j).get(4)
							+ " ** " + map.get("promoEndDate"));
					System.out.println("promoUsage db: " + items.get(j).get(5)
							+ " ** " + map.get("promoUsage"));
					System.out.println("criteria_id db: " + items.get(j).get(6)
							+ " ** " + map.get("criteriaId"));
					System.out.println("bonus_type db: " + items.get(j).get(7)
							+ " ** " + map.get("bonusType"));
					System.out.println("bonus_amount db: " + items.get(j).get(8)
							+ " ** " + map.get("bonusAmount"));
					System.out.println("min_deposit db: " + items.get(j).get(9)
							+ " ** " + map.get("minDeposit"));
					System.out.println("percentage db: " + items.get(j).get(10)
							+ " ** " + map.get("percentage"));
					System.out.println("max_bonus_amount db: "
							+ items.get(j).get(11) + " ** "
							+ map.get("maxBonusAmount"));
					System.out.println("chunks db: " + items.get(j).get(12)
							+ " ** " + map.get("chunks"));
					System.out.println("wager db: " + items.get(j).get(13) + " ** "
							+ map.get("wager"));
					System.out.println("bonus_expiry_days db: "
							+ items.get(j).get(14) + " ** "
							+ map.get("bonusExpiryDays"));
					System.out.println("promo_code_status db: "
							+ items.get(j).get(15) + " ** "
							+ map.get("promoCodeStatus"));

				}

			}

			catch (RCException e) {
				System.out.println("in exception");
				Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
			}

		}// end of getrewardstorebonus

		@Test
		public void IssueBonus() {
			String path = ServicesEndPoint.ISSUE_BONUS;
			BonusService ss = new BonusService();
			System.out.println("path is " + path);

			String uid = prop.getProperty("USERID");
			long userId = Long.parseLong(uid);

			String promoid = prop.getProperty("PROMOCODEID");
			long promoCodeId = Long.parseLong(promoid);

			String prc = prop.getProperty("PRICE");
			long price = Long.parseLong(prc);

			ss.setUserId(userId);
			ss.setPromoCodeId(promoCodeId);
			ss.setPrice(price);

			HTTPRequest request = new HTTPRequest(ss);

			HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

			List<HashMap> details = JsonPath
					.parse(httpResp.getBody().getBodyText()).read("$.[*]");

			System.out.println("RESPONSE : " + httpResp.getBody().getBodyText());
			System.out.println("RESPONSE STATUS: " + httpResp.getStatusCode());

			try {

				List<List<Object>> items = bonusdao.issueBonus(userId, promoCodeId);

				for (int j = 0; j < items.size(); j++) {

					// HashMap map = details.get(j);
					System.out.println("id: " + items.get(j).get(0));
					System.out
							.println("bonus_promotion_id: " + items.get(j).get(1));
					System.out.println("user_id: " + items.get(j).get(2));
					System.out.println("order_id: " + items.get(j).get(3));
					System.out.println("is_payment_authorised: "
							+ items.get(j).get(4));
					System.out.println("total_chunks: " + items.get(j).get(5));
					System.out.println("chunks_moved: " + items.get(j).get(6));
					System.out
							.println("total_bonus_amount: " + items.get(j).get(7));
					System.out.println("bonus_amount_perchunk: "
							+ items.get(j).get(8));
					System.out.println("bonus_amount_released: "
							+ items.get(j).get(9));
					System.out.println("total_entryfee: " + items.get(j).get(10));
					System.out.println("total_entryfee_played: "
							+ items.get(j).get(11));
					System.out
							.println("entryfee_perchunk: " + items.get(j).get(12));
					System.out.println("bonus_start_date: " + items.get(j).get(13));
					System.out.println("bonus_end_date: " + items.get(j).get(14));
					System.out
							.println("bonus_expiry_date: " + items.get(j).get(15));
					System.out.println("status: " + items.get(j).get(16));
					System.out.println("issued_by: " + items.get(j).get(17));
					System.out.println("update_date: " + items.get(j).get(18));

				}
			} catch (RCException e) {
				System.out.println("in exception");
				Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
			}

		} // 

		

}
