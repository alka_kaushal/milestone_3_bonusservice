protocol=$1
host_new=$2
port_new=$3
testfile=$4


echo "protocol set to : ${protocol}"
echo "host is set to: ${host_new}"
echo "port is set to : ${port_new}"
echo "yml test file to execute is set to : ${testfile}"



sed -i -e "s/protocol=/protocol=${protocol}/g" src/test/resources/config.properties
sed -i -e "s/host=/host=${host_new}/g" src/test/resources/config.properties
sed -i -e "s/port=/port=${port_new}/g" src/test/resources/config.properties
sed -i -e "s/test.filename=/test.filename=${testfile}/g" src/test/resources/config.properties




