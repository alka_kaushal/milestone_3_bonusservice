package com.rummycircle.BonusServiceM3;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.rummycircle.BonusServiceM3DAO;
import com.rummycircle.ServicesEndPoint;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.rp.NfsRpTest;
import com.rummycircle.rp.RP_LPServiceClass;
import com.rummycircle.ticket.TicketService;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycircle.utils.testutils.PropertyReader;

public class BonusServiceM3Test extends BaseTest {

	BonusServiceM3DAO dbObj = new BonusServiceM3DAO();
	private static Logger Log = Logger.getLogger(BonusServiceM3Test.class);
	Properties prop = PropertyReader.loadCustomProperties("custom.properties");

	//@Test
	public void getPromoCodes() {

		int responseSize = 0;
		int countFromDB = 0;
		String path;
		Object httpReqObj = null;
		int promoType = Integer.parseInt(prop.getProperty("IntpromoType")
				.trim());

		if (promoType == 0) {
			path = ServicesEndPoint.PROMO_CODES_LIST;
		} else {
			path = ServicesEndPoint.PROMO_CODES_LIST + "?promoType="
					+ promoType;
		}

		HTTPRequest httpRequest = new HTTPRequest(httpReqObj);
		HTTPResponse HTTPresponse = rc.sendRequest(HTTPMethod.GET, path,
				httpRequest);

		List<HashMap> responseBody = JsonPath.parse(
				HTTPresponse.getBody().getBodyText()).read("$.[*]");
		List<List<Object>> promoCodesDB = dbObj.getPromoCodes(promoType);
		responseSize = responseBody.size();
		countFromDB = dbObj.getCountPromoCode(promoType);

		// Assertions for checking if the same response size has been received
		Assert.assertEquals(countFromDB, responseSize);

		// Assertions with expected values as DB values and actual as the
		// responseBody values
		for (int i = 0; i < countFromDB; i++) {
			HashMap Responsemap = responseBody.get(i);

			Assert.assertEquals(promoCodesDB.get(i).get(0),
					Responsemap.get("id"));
			Assert.assertEquals(promoCodesDB.get(i).get(1),
					Responsemap.get("promotionType"));
			Assert.assertEquals(promoCodesDB.get(i).get(2),
					Responsemap.get("promotionCode"));
			Assert.assertEquals(promoCodesDB.get(i).get(3),
					Responsemap.get("promoStartDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(4),
					Responsemap.get("promoEndDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(5),
					Responsemap.get("promoUsage"));
			Assert.assertEquals(promoCodesDB.get(i).get(6),
					Responsemap.get("criteriaId"));
			Assert.assertEquals(promoCodesDB.get(i).get(7),
					Responsemap.get("bonusType"));
			Assert.assertEquals(promoCodesDB.get(i).get(8),
					Responsemap.get("bonusAmount"));
			Assert.assertEquals(promoCodesDB.get(i).get(9),
					Responsemap.get("minDeposit"));
			Assert.assertEquals(promoCodesDB.get(i).get(10),
					Responsemap.get("percentage"));
			Assert.assertEquals(promoCodesDB.get(i).get(11),
					Responsemap.get("maxBonusAmount"));
			Assert.assertEquals(promoCodesDB.get(i).get(12),
					Responsemap.get("chunks"));
			Assert.assertEquals(promoCodesDB.get(i).get(13),
					Responsemap.get("wager"));
			Assert.assertEquals(promoCodesDB.get(i).get(14),
					Responsemap.get("bonusExpiryDays"));
			Assert.assertEquals(promoCodesDB.get(i).get(15),
					Responsemap.get("promoCodeStatus"));
			Assert.assertEquals(promoCodesDB.get(i).get(16),
					Responsemap.get("createdBy"));
			Assert.assertEquals(promoCodesDB.get(i).get(17),
					Responsemap.get("creationDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(18),
					Responsemap.get("lastUpdateBy"));
			Assert.assertEquals(promoCodesDB.get(i).get(19),
					Responsemap.get("lastUpdateDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(20),
					Responsemap.get("campaignId"));
			Assert.assertEquals(promoCodesDB.get(i).get(21),
					Responsemap.get("termsConditionsLink"));
			Assert.assertEquals(promoCodesDB.get(i).get(22),
					Responsemap.get("channelId"));
			Assert.assertEquals(promoCodesDB.get(i).get(23),
					Responsemap.get("isDisplayed"));
			Assert.assertEquals(promoCodesDB.get(i).get(24),
					Responsemap.get("limitedBonusType"));

		}
		System.out.println("Assertions passed all promo codes");
	}

	//@Test
	public void createPromoCode() {
		String path = ServicesEndPoint.CREATE_PROMO_CODE;
		PromoCodeServiceClass serviceObj = new PromoCodeServiceClass();
		campaignPromo campaignObj = new campaignPromo();
		limitedBonus limitedBonusObj = new limitedBonus();

		serviceObj.setPromoType(Integer.parseInt(prop
				.getProperty("IntpromoType")));
		serviceObj.setPromoCode(prop.getProperty("promoCode").trim());
		serviceObj.setPromoStartDate(prop.getProperty("promoStartDate").trim());
		serviceObj.setPromoEndDate(prop.getProperty("promoEndDate").trim());
		serviceObj.setPromoUsage(Integer.parseInt(prop
				.getProperty("promoUsage").trim()));
		serviceObj.setCriteriaId(Integer.parseInt(prop
				.getProperty("criteriaId").trim()));
		serviceObj.setBonusType(Integer.parseInt(prop.getProperty("bonusType")
				.trim()));
		serviceObj.setBonusAmount(Double.parseDouble(prop.getProperty(
				"bonusAmount").trim()));
		serviceObj.setMinDeposit(Double.parseDouble(prop.getProperty(
				"minDeposit").trim()));
		serviceObj.setPercentage(Double.parseDouble(prop.getProperty(
				"percentage").trim()));
		serviceObj.setMaxBonusAmount(Double.parseDouble(prop.getProperty(
				"maxBonusAmount").trim()));
		serviceObj.setChunks(Integer.parseInt(prop.getProperty("chunks")));
		serviceObj.setWager(Double
				.parseDouble(prop.getProperty("wager").trim()));
		serviceObj.setBonusExpiryDays(Integer.parseInt(prop.getProperty(
				"bonusExpiryDays").trim()));
		serviceObj.setPromoCodeStatus(Integer.parseInt(prop.getProperty(
				"promoCodeStatus").trim()));
		serviceObj.setCreatedBy(Integer.parseInt(prop.getProperty("createdBy")
				.trim()));
		serviceObj.setCreationDate(prop.getProperty("creationDate").trim());
		serviceObj.setLastUpdateBy(Integer.parseInt(prop.getProperty(
				"lastUpdateBy").trim()));
		serviceObj.setLastUpdateDate(prop.getProperty("lastUpdateDate").trim());
		serviceObj.setCampaignId(Integer.parseInt(prop
				.getProperty("campaignId").trim()));
		serviceObj.setTermsConditionsLink(prop.getProperty(
				"termsConditionsLink").trim());
		serviceObj.setChannelId(Integer.parseInt(prop.getProperty("channelId")
				.trim()));
		serviceObj.setIsDisplayed(Integer.parseInt(prop.getProperty(
				"isDisplayed").trim()));
		serviceObj.setLimitedBonusType(Integer.parseInt(prop.getProperty(
				"limitedBonusType").trim()));
		// Tokenize the string using , as the token and converting it into a
		// List of strings
		List<String> uniquePromoCodes = Stringtokenizer.tokenize(
				prop.getProperty("uniquePromoCodes"), ",");
		serviceObj.setUniquePromoCodes(uniquePromoCodes);
		campaignObj.setCampaignName(prop.getProperty("campaignName"));
		campaignObj.setOfferType(Integer.parseInt(prop.getProperty("offerType")
				.trim()));
		campaignObj.setStatus(Integer.parseInt(prop.getProperty("status")
				.trim()));
		serviceObj.setCampaignPromo(campaignObj);
		limitedBonusObj.setLimitedQuantity(Integer.parseInt(prop.getProperty(
				"limitedQuantity").trim()));
		limitedBonusObj.setDisplayHrs(prop.getProperty("displayHrs").trim());
		limitedBonusObj
				.setReservedTime(prop.getProperty("reservedTime").trim());
		limitedBonusObj.setTicketId(Integer.parseInt(prop.getProperty(
				"ticketId").trim()));
		limitedBonusObj.setMessageTxt(prop.getProperty("messageTxt").trim());
		limitedBonusObj.setMessageStartTime(prop
				.getProperty("messageStartTime").trim());
		serviceObj.setLimitedBonus(limitedBonusObj);

		HTTPRequest httpRequest = new HTTPRequest(serviceObj);
		HTTPResponse HTTPresponse = rc.sendRequest(HTTPMethod.POST, path,
				httpRequest);

		List<List<Object>> bonusPromotionsData = dbObj
				.getPromoBonusPromotions(serviceObj.getPromoCode());
		List<List<Object>> campaignPromotionsData = dbObj
				.getCampaignPromotions(serviceObj.getId());
		List<List<Object>> limitedBonusPromoData = dbObj
				.getLimitedbonuspromotions(serviceObj.getId());

		Assert.assertEquals(HTTPresponse.getStatusCode(),200);

		Assert.assertEquals(bonusPromotionsData.get(0).get(1),
				serviceObj.getPromoType());
		Assert.assertEquals(bonusPromotionsData.get(0).get(2),
				serviceObj.getPromoCode());
		Assert.assertEquals(bonusPromotionsData.get(0).get(2),
				serviceObj.getPromoStartDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(3),
				serviceObj.getPromoEndDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(4),
				serviceObj.getPromoUsage());
		Assert.assertEquals(bonusPromotionsData.get(0).get(5),
				serviceObj.getCriteriaId());
		Assert.assertEquals(bonusPromotionsData.get(0).get(6),
				serviceObj.getBonusType());
		Assert.assertEquals(bonusPromotionsData.get(0).get(7),
				serviceObj.getBonusAmount());
		Assert.assertEquals(bonusPromotionsData.get(0).get(8),
				serviceObj.getMinDeposit());
		Assert.assertEquals(bonusPromotionsData.get(0).get(9),
				serviceObj.getPercentage());
		Assert.assertEquals(bonusPromotionsData.get(0).get(10),
				serviceObj.getMaxBonusAmount());
		Assert.assertEquals(bonusPromotionsData.get(0).get(11),
				serviceObj.getChunks());
		Assert.assertEquals(bonusPromotionsData.get(0).get(12),
				serviceObj.getWager());
		Assert.assertEquals(bonusPromotionsData.get(0).get(13),
				serviceObj.getBonusExpiryDays());
		Assert.assertEquals(bonusPromotionsData.get(0).get(14),
				serviceObj.getPromoCodeStatus());
		Assert.assertEquals(bonusPromotionsData.get(0).get(15),
				serviceObj.getCreatedBy());
		Assert.assertEquals(bonusPromotionsData.get(0).get(16),
				serviceObj.getCreationDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(17),
				serviceObj.getLastUpdateBy());
		Assert.assertEquals(bonusPromotionsData.get(0).get(18),
				serviceObj.getLastUpdateDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(19),
				serviceObj.getCampaignId());
		Assert.assertEquals(bonusPromotionsData.get(0).get(20),
				serviceObj.getTermsConditionsLink());
		Assert.assertEquals(bonusPromotionsData.get(0).get(21),
				serviceObj.getChannelId());
		Assert.assertEquals(bonusPromotionsData.get(0).get(22),
				serviceObj.getIsDisplayed());
		Assert.assertEquals(bonusPromotionsData.get(0).get(23),
				serviceObj.getLimitedBonusType());

		Assert.assertEquals(campaignPromotionsData.get(0).get(0),
				campaignObj.getCampaignName());
		Assert.assertEquals(campaignPromotionsData.get(0).get(1),
				campaignObj.getOfferType());
		Assert.assertEquals(campaignPromotionsData.get(0).get(2),
				campaignObj.getStatus());

		Assert.assertEquals(limitedBonusPromoData.get(0).get(0),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(1),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(2),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(3),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(4),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(5),
				limitedBonusObj.getLimitedQuantity());

		System.out.println("Promocode created succesfully!!");
	}

	//@Test
	public void searchPromoCode() {
		String path = null;
		Object httpReqObj = null;
		List<List<Object>> promoCodesDB = null;

		if (Integer.parseInt(prop.getProperty("searchByPromoName").trim()) == 1) {
			path = ServicesEndPoint.SEARCH_PROMO_CODE + "?promoCode="
					+ prop.getProperty("promoNameToSearch").trim();
			promoCodesDB = dbObj.getPromoBonusPromotions(prop.getProperty(
					"promoNameToSearch").trim());
		} else {
			// If status, createdBy, promoType are sent with the request 
			if (Integer.parseInt(prop.getProperty("searchAddingstatus").trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 1) {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&status="
						+ prop.getProperty("status").trim() + "&createdBy="
						+ prop.getProperty("createdBy").trim() + "&promoType="
						+ prop.getProperty("promoType").trim();

				promoCodesDB = dbObj.getBonusPromotionsAllOptions(Integer
						.parseInt(prop.getProperty("IntDataType").trim()), prop
						.getProperty("fromDate").trim(),
						prop.getProperty("toDate").trim(), Integer
								.parseInt(prop.getProperty("status").trim()),
						Integer.parseInt(prop.getProperty("createdBy").trim()),
						Integer.parseInt(prop.getProperty("IntpromoType")
								.trim()));

			} 
			// If status, createdBy are sent with the request 
			else if (Integer.parseInt(prop.getProperty("searchAddingstatus")
					.trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 0) {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&status="
						+ prop.getProperty("status").trim() + "&createdBy="
						+ prop.getProperty("createdBy").trim();

				promoCodesDB = dbObj
						.getPromoBonusPromotionsStatusCreatedBy(Integer
								.parseInt(prop.getProperty("IntDataType")
										.trim()), prop.getProperty("fromDate")
								.trim(), prop.getProperty("toDate").trim(),
								Integer.parseInt(prop.getProperty("status")
										.trim()), Integer.parseInt(prop
										.getProperty("createdBy").trim()));

			} 
			// If status, promoType are sent with the request 
			else if (Integer.parseInt(prop.getProperty("searchAddingstatus")
					.trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 0
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 1) {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&status="
						+ prop.getProperty("status").trim() + "&promoType="
						+ prop.getProperty("promoType").trim();

				promoCodesDB = dbObj
						.getPromoBonusPromotionsfromStatusPromoType(Integer
								.parseInt(prop.getProperty("IntdateType")
										.trim()), prop.getProperty("fromDate")
								.trim(), prop.getProperty("toDate").trim(),
								Integer.parseInt(prop.getProperty("status")
										.trim()), Integer.parseInt(prop
										.getProperty("IntpromoType").trim()));

			} 
			// If only status is sent with the request 
			else if (Integer.parseInt(prop.getProperty("searchAddingstatus")
					.trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 0
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 0) {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&status="
						+ prop.getProperty("status").trim();

				promoCodesDB = dbObj.getPromoBonusPromotionsFromStatus(Integer
						.parseInt(prop.getProperty("IntDateType").trim()), prop
						.getProperty("fromDate").trim(),
						prop.getProperty("toDate").trim(), Integer
								.parseInt(prop.getProperty("status").trim()));

			} 
			// If createdBy, promoType are sent with the request 
			else if (Integer.parseInt(prop.getProperty("searchAddingstatus")
					.trim()) == 0
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 1) {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&createdBy="
						+ prop.getProperty("createdBy").trim() + "&promoType="
						+ prop.getProperty("promoType").trim();

				promoCodesDB = dbObj
						.getPromoBonusPromotionsfromPromoTypeCreatedBy(Integer
								.parseInt(prop.getProperty("IntDateKey")), prop
								.getProperty("fromDate").trim(), prop
								.getProperty("toDate").trim(),
								Integer.parseInt(prop.getProperty("createdBy")
										.trim()), Integer.parseInt(prop
										.getProperty("IntpromoType").trim()));

			}
			// If only createdBy is sent with the request 
			else if (Integer.parseInt(prop.getProperty("searchAddingstatus")
					.trim()) == 0
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 1
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 0) {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&createdBy="
						+ prop.getProperty("createdBy").trim();

				promoCodesDB = dbObj
						.getPromoBonusPromotionsfromCreatedBy(Integer
								.parseInt(prop.getProperty("IntDateType")
										.trim()), prop.getProperty("fromDate")
								.trim(), prop.getProperty("toDate").trim(),
								Integer.parseInt(prop.getProperty("createdBy")
										.trim()));

			} 
			// If only promoType is sent with the request 
			else if (Integer.parseInt(prop.getProperty("searchAddingstatus")
					.trim()) == 0
					&& Integer.parseInt(prop.getProperty(
							"searchAddingcreatedBy").trim()) == 0
					&& Integer.parseInt(prop.getProperty(
							"searchAddingpromoType").trim()) == 1) {

				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim() + "&promoType="
						+ prop.getProperty("promoType").trim();

				promoCodesDB = dbObj
						.getPromoBonusPromotionsfromPromoType(Integer
								.parseInt(prop.getProperty("IntDateType")
										.trim()), prop.getProperty("fromDate")
								.trim(), prop.getProperty("toDate").trim(),
								Integer.parseInt(prop.getProperty(
										"IntpromoType").trim()));

			} 
			// If only the mandatory date fields are sent with the request
			else {
				path = ServicesEndPoint.SEARCH_PROMO_CODE + "?dateType="
						+ prop.getProperty("dateType").trim() + "&fromDate="
						+ prop.getProperty("fromDate").trim() + "&toDate="
						+ prop.getProperty("toDate").trim();

				promoCodesDB = dbObj
						.getPromoBonusPromotionsfromDateType(Integer
								.parseInt(prop.getProperty("IntDateType")
										.trim()), prop.getProperty("fromDate")
								.trim(), prop.getProperty("toDate").trim());

			}
		}

		HTTPRequest httpRequest = new HTTPRequest(httpReqObj);
		HTTPResponse HTTPresponse = rc.sendRequest(HTTPMethod.GET, path,
				httpRequest);

		List<HashMap> responseBody = JsonPath.parse(
				HTTPresponse.getBody().getBodyText()).read("$.[*]");

		int responseSize = responseBody.size();

		// Iterating through the response received to assert from DB
		for (int i = 0; i < responseSize; i++) {
			HashMap Responsemap = responseBody.get(i);

			Assert.assertEquals(promoCodesDB.get(i).get(0),
					Responsemap.get("id"));
			Assert.assertEquals(promoCodesDB.get(i).get(1),
					Responsemap.get("promotionType"));
			Assert.assertEquals(promoCodesDB.get(i).get(2),
					Responsemap.get("promotionCode"));
			Assert.assertEquals(promoCodesDB.get(i).get(3),
					Responsemap.get("promoStartDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(4),
					Responsemap.get("promoEndDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(5),
					Responsemap.get("promoUsage"));
			Assert.assertEquals(promoCodesDB.get(i).get(6),
					Responsemap.get("criteriaId"));
			Assert.assertEquals(promoCodesDB.get(i).get(7),
					Responsemap.get("bonusType"));
			Assert.assertEquals(promoCodesDB.get(i).get(8),
					Responsemap.get("bonusAmount"));
			Assert.assertEquals(promoCodesDB.get(i).get(9),
					Responsemap.get("minDeposit"));
			Assert.assertEquals(promoCodesDB.get(i).get(10),
					Responsemap.get("percentage"));
			Assert.assertEquals(promoCodesDB.get(i).get(11),
					Responsemap.get("maxBonusAmount"));
			Assert.assertEquals(promoCodesDB.get(i).get(12),
					Responsemap.get("chunks"));
			Assert.assertEquals(promoCodesDB.get(i).get(13),
					Responsemap.get("wager"));
			Assert.assertEquals(promoCodesDB.get(i).get(14),
					Responsemap.get("bonusExpiryDays"));
			Assert.assertEquals(promoCodesDB.get(i).get(15),
					Responsemap.get("promoCodeStatus"));
			Assert.assertEquals(promoCodesDB.get(i).get(16),
					Responsemap.get("createdBy"));
			Assert.assertEquals(promoCodesDB.get(i).get(17),
					Responsemap.get("creationDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(18),
					Responsemap.get("lastUpdateBy"));
			Assert.assertEquals(promoCodesDB.get(i).get(19),
					Responsemap.get("lastUpdateDate"));
			Assert.assertEquals(promoCodesDB.get(i).get(20),
					Responsemap.get("campaignId"));
			Assert.assertEquals(promoCodesDB.get(i).get(21),
					Responsemap.get("termsConditionsLink"));
			Assert.assertEquals(promoCodesDB.get(i).get(22),
					Responsemap.get("channelId"));
			Assert.assertEquals(promoCodesDB.get(i).get(23),
					Responsemap.get("isDisplayed"));
			Assert.assertEquals(promoCodesDB.get(i).get(24),
					Responsemap.get("limitedBonusType"));

		}

		System.out.println("Searched all promo codes successfully!!");
	}

	@Test
	public void updatePromoCode() {
		String path = ServicesEndPoint.UPDATE_PROMO_CODE;
		PromoCodeServiceClass serviceObj = new PromoCodeServiceClass();
		campaignPromo campaignObj = new campaignPromo();
		limitedBonus limitedBonusObj = new limitedBonus();

		serviceObj.setPromoType(Integer.parseInt(prop
				.getProperty("IntpromoType")));
		serviceObj.setPromoCode(prop.getProperty("promoCode").trim());
		serviceObj.setPromoStartDate(prop.getProperty("promoStartDate").trim());
		serviceObj.setPromoEndDate(prop.getProperty("promoEndDate").trim());
		serviceObj.setPromoUsage(Integer.parseInt(prop
				.getProperty("promoUsage").trim()));
		serviceObj.setCriteriaId(Integer.parseInt(prop
				.getProperty("criteriaId").trim()));
		serviceObj.setBonusType(Integer.parseInt(prop.getProperty("bonusType")
				.trim()));
		serviceObj.setBonusAmount(Double.parseDouble(prop.getProperty(
				"bonusAmount").trim()));
		serviceObj.setMinDeposit(Double.parseDouble(prop.getProperty(
				"minDeposit").trim()));
		serviceObj.setPercentage(Double.parseDouble(prop.getProperty(
				"percentage").trim()));
		serviceObj.setMaxBonusAmount(Double.parseDouble(prop.getProperty(
				"maxBonusAmount").trim()));
		serviceObj.setChunks(Integer.parseInt(prop.getProperty("chunks")));
		serviceObj.setWager(Double
				.parseDouble(prop.getProperty("wager").trim()));
		serviceObj.setBonusExpiryDays(Integer.parseInt(prop.getProperty(
				"bonusExpiryDays").trim()));
		serviceObj.setPromoCodeStatus(Integer.parseInt(prop.getProperty(
				"promoCodeStatus").trim()));
		serviceObj.setCreatedBy(Integer.parseInt(prop.getProperty("createdBy")
				.trim()));
		serviceObj.setCreationDate(prop.getProperty("creationDate").trim());
		serviceObj.setLastUpdateBy(Integer.parseInt(prop.getProperty(
				"lastUpdateBy").trim()));
		serviceObj.setLastUpdateDate(prop.getProperty("lastUpdateDate").trim());
		serviceObj.setCampaignId(Integer.parseInt(prop
				.getProperty("campaignId").trim()));
		serviceObj.setTermsConditionsLink(prop.getProperty(
				"termsConditionsLink").trim());
		serviceObj.setChannelId(Integer.parseInt(prop.getProperty("channelId")
				.trim()));
		serviceObj.setIsDisplayed(Integer.parseInt(prop.getProperty(
				"isDisplayed").trim()));
		serviceObj.setLimitedBonusType(Integer.parseInt(prop.getProperty(
				"limitedBonusType").trim()));

		// Take promo codes from the property file and convert into a list
		List<String> uniquePromoCodes = Stringtokenizer.tokenize(
				prop.getProperty("uniquePromoCodes"), ",");
		serviceObj.setUniquePromoCodes(uniquePromoCodes);

		campaignObj.setCampaignName(prop.getProperty("campaignName"));
		campaignObj.setOfferType(Integer.parseInt(prop.getProperty("offerType")
				.trim()));
		campaignObj.setStatus(Integer.parseInt(prop.getProperty("status")
				.trim()));
		serviceObj.setCampaignPromo(campaignObj);

		limitedBonusObj.setLimitedQuantity(Integer.parseInt(prop.getProperty(
				"limitedQuantity").trim()));
		limitedBonusObj.setDisplayHrs(prop.getProperty("displayHrs").trim());
		limitedBonusObj
				.setReservedTime(prop.getProperty("reservedTime").trim());
		limitedBonusObj.setTicketId(Integer.parseInt(prop.getProperty(
				"ticketId").trim()));
		limitedBonusObj.setMessageTxt(prop.getProperty("messageTxt").trim());
		limitedBonusObj.setMessageStartTime(prop
				.getProperty("messageStartTime").trim());
		serviceObj.setLimitedBonus(limitedBonusObj);

		HTTPRequest httpRequest = new HTTPRequest(serviceObj);
		HTTPResponse HTTPresponse = rc.sendRequest(HTTPMethod.POST, path,
				httpRequest);

		// Getting data from DB in list objects to iterate through
		List<List<Object>> bonusPromotionsData = dbObj
				.getPromoBonusPromotions(serviceObj.getPromoCode());
		List<List<Object>> campaignPromotionsData = dbObj
				.getCampaignPromotions(serviceObj.getId());
		List<List<Object>> limitedBonusPromoData = dbObj
				.getLimitedbonuspromotions(serviceObj.getId());

		Assert.assertEquals(HTTPresponse.getStatusCode(),200);

		// Assertions from bonus_promotions table
		Assert.assertEquals(bonusPromotionsData.get(0).get(1),
				serviceObj.getPromoType());
		Assert.assertEquals(bonusPromotionsData.get(0).get(2),
				serviceObj.getPromoCode());
		Assert.assertEquals(bonusPromotionsData.get(0).get(2),
				serviceObj.getPromoStartDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(3),
				serviceObj.getPromoEndDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(4),
				serviceObj.getPromoUsage());
		Assert.assertEquals(bonusPromotionsData.get(0).get(5),
				serviceObj.getCriteriaId());
		Assert.assertEquals(bonusPromotionsData.get(0).get(6),
				serviceObj.getBonusType());
		Assert.assertEquals(bonusPromotionsData.get(0).get(7),
				serviceObj.getBonusAmount());
		Assert.assertEquals(bonusPromotionsData.get(0).get(8),
				serviceObj.getMinDeposit());
		Assert.assertEquals(bonusPromotionsData.get(0).get(9),
				serviceObj.getPercentage());
		Assert.assertEquals(bonusPromotionsData.get(0).get(10),
				serviceObj.getMaxBonusAmount());
		Assert.assertEquals(bonusPromotionsData.get(0).get(11),
				serviceObj.getChunks());
		Assert.assertEquals(bonusPromotionsData.get(0).get(12),
				serviceObj.getWager());
		Assert.assertEquals(bonusPromotionsData.get(0).get(13),
				serviceObj.getBonusExpiryDays());
		Assert.assertEquals(bonusPromotionsData.get(0).get(14),
				serviceObj.getPromoCodeStatus());
		Assert.assertEquals(bonusPromotionsData.get(0).get(15),
				serviceObj.getCreatedBy());
		Assert.assertEquals(bonusPromotionsData.get(0).get(16),
				serviceObj.getCreationDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(17),
				serviceObj.getLastUpdateBy());
		Assert.assertEquals(bonusPromotionsData.get(0).get(18),
				serviceObj.getLastUpdateDate());
		Assert.assertEquals(bonusPromotionsData.get(0).get(19),
				serviceObj.getCampaignId());
		Assert.assertEquals(bonusPromotionsData.get(0).get(20),
				serviceObj.getTermsConditionsLink());
		Assert.assertEquals(bonusPromotionsData.get(0).get(21),
				serviceObj.getChannelId());
		Assert.assertEquals(bonusPromotionsData.get(0).get(22),
				serviceObj.getIsDisplayed());
		Assert.assertEquals(bonusPromotionsData.get(0).get(23),
				serviceObj.getLimitedBonusType());

		// Assertions from campaign_promotions table
		Assert.assertEquals(campaignPromotionsData.get(0).get(0),
				campaignObj.getCampaignName());
		Assert.assertEquals(campaignPromotionsData.get(0).get(1),
				campaignObj.getOfferType());
		Assert.assertEquals(campaignPromotionsData.get(0).get(2),
				campaignObj.getStatus());

		// Assertions from limited_bonus_promotions table
		Assert.assertEquals(limitedBonusPromoData.get(0).get(0),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(1),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(2),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(3),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(4),
				limitedBonusObj.getLimitedQuantity());
		Assert.assertEquals(limitedBonusPromoData.get(0).get(5),
				limitedBonusObj.getLimitedQuantity());

		System.out.println("Promocode updated succesfully!!");

	}

	// @Test
	public void getByOrderId() {
		String path = null;
		Object httpReqObj = null;
		List<List<Object>> promoCodesDB = null;
		path = ServicesEndPoint.GET_BY_ORDERID + "?orderId="
				+ prop.getProperty("orderId").trim();

		HTTPRequest httpRequest = new HTTPRequest(httpReqObj);
		HTTPResponse HTTPresponse = rc.sendRequest(HTTPMethod.GET, path,
				httpRequest);
		int recievedStatusCode = HTTPresponse.getStatusCode();
		Assert.assertEquals(200, recievedStatusCode);

		System.out.println("Order id compared");
	}
}
