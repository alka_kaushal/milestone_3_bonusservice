package com.rummycircle.rp;

public class CreditOnVerificationServiceClass {

	private long userId;
	private long tournamentId;
	private int sysAccId;
	private long txnID;
	private long rewardPointValue;
	private double newrpSpendCounter;
	//private int clubType;
	private String comment;
	private int creditDebit;
	private long rewardStoreItemId;
	private double rewardStorePrice;
	private double rupeeValue;
	
	public long getTxnID() {
		return txnID;
	}

	public void setTxnID(long txnID) {
		this.txnID = txnID;
	}

	public double getRupeeValue() {
		return rupeeValue;
	}

	public void setRupeeValue(double rupeeValue) {
		this.rupeeValue = rupeeValue;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(long tournamentId) {
		this.tournamentId = tournamentId;
	}

	public int getSysAccId() {
		return sysAccId;
	}

	public void setSysAccId(int sysAccId) {
		this.sysAccId = sysAccId;
	}

	public long gettxnID() {
		return txnID;
	}

	public void settxnID(long txnID) {
		this.txnID = txnID;
	}

	public long getRewardPointValue() {
		return rewardPointValue;
	}

	public void setRewardPointValue(long rewardPointValue) {
		this.rewardPointValue = rewardPointValue;
	}

	public String getComment() {
		return comment;
	}

	public double getNewrpSpendCounter() {
		return newrpSpendCounter;
	}

	public void setNewrpSpendCounter(double newrpSpendCounter) {
		this.newrpSpendCounter = newrpSpendCounter;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getCreditDebit() {
		return creditDebit;
	}

	public void setCreditDebit(int creditDebit) {
		this.creditDebit = creditDebit;
	}

	public long getRewardStoreItemId() {
		return rewardStoreItemId;
	}

	public void setRewardStoreItemId(long rewardStoreItemId) {
		this.rewardStoreItemId = rewardStoreItemId;
	}

	public double getRewardStorePrice() {
		return rewardStorePrice;
	}

	public void setRewardStorePrice(double rewardStorePrice) {
		this.rewardStorePrice = rewardStorePrice;
	}



	@Override
	public String toString() {
		return "CreditOnVerificationServiceClass [userId=" + userId
				+ ", tournamentId=" + tournamentId + ", sysAccId=" + sysAccId
				+ ", txnID=" + txnID + ", rewardPointValue=" + rewardPointValue
				+ ", newrpSpendCounter=" + newrpSpendCounter + ", comment="
				+ comment + ", creditDebit=" + creditDebit
				+ ", rewardStoreItemId=" + rewardStoreItemId
				+ ", rewardStorePrice=" + rewardStorePrice + ", rupeeValue="
				+ rupeeValue + "]";
	}
}


