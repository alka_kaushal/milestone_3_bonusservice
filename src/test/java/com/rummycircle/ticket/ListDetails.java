package com.rummycircle.ticket;

public class ListDetails {

	int id;
	String name;
	int ticketValue;
	short ticketType;
	short expiryType;
	String expiryDate;
	boolean displayToUsers;
	short ticketStatus;
	String createdBy;
	String createdOn;
	String updateDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTicketValue() {
		return ticketValue;
	}

	public void setTicketValue(int ticketValue) {
		this.ticketValue = ticketValue;
	}

	public short getTicketType() {
		return ticketType;
	}

	public void setTicketType(short ticketType) {
		this.ticketType = ticketType;
	}

	public short getExpiryType() {
		return expiryType;
	}

	public void setExpiryType(short expiryType) {
		this.expiryType = expiryType;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public boolean isDisplayToUsers() {
		return displayToUsers;
	}

	public void setDisplayToUsers(boolean displayToUsers) {
		this.displayToUsers = displayToUsers;
	}

	public short getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(short ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;

	}

}
