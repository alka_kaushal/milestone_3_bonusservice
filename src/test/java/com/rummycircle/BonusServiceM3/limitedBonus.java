package com.rummycircle.BonusServiceM3;

public class limitedBonus {

	private int limitedQuantity;
	private String displayHrs;
	private String reservedTime;
	private int ticketId;
	private String messageTxt;
	private String messageStartTime;

	public int getLimitedQuantity() {
		return limitedQuantity;
	}

	public void setLimitedQuantity(int limitedQuantity) {
		this.limitedQuantity = limitedQuantity;
	}

	public String getDisplayHrs() {
		return displayHrs;
	}

	public void setDisplayHrs(String displayHrs) {
		this.displayHrs = displayHrs;
	}

	public String getReservedTime() {
		return reservedTime;
	}

	public void setReservedTime(String reservedTime) {
		this.reservedTime = reservedTime;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getMessageStartTime() {
		return messageStartTime;
	}

	public void setMessageStartTime(String messageStartTime) {
		this.messageStartTime = messageStartTime;
	}

	@Override
	public String toString() {
		return "limitedBonus [limitedQuantity=" + limitedQuantity
				+ ", displayHrs=" + displayHrs + ", reservedTime="
				+ reservedTime + ", ticketId=" + ticketId + ", messageTxt="
				+ messageTxt + ", messageStartTime=" + messageStartTime + "]";
	}

}
