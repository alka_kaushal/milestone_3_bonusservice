package com.rummycircle.BonusServiceM3;

import java.util.Arrays;
import java.util.List;

public class PromoCodeServiceClass {

	private int id;
	private int promoType;
	private String promoCode;
	private String promoStartDate;
	private String promoEndDate;
	private int promoUsage;
	private int criteriaId;
	private int bonusType;
	private double bonusAmount;
	private double minDeposit;
	private double percentage;
	private double maxBonusAmount;
	private int chunks;
	private double wager;
	private int bonusExpiryDays;
	private int promoCodeStatus;
	private int createdBy;
	private String creationDate;
	private int lastUpdateBy;
	private String lastUpdateDate;
	private int campaignId;
	private String termsConditionsLink;
	private int channelId;
	private int isDisplayed;
	private int limitedBonusType;
	private List<String> uniquePromoCodes;
	private campaignPromo campaignPromo;
	private limitedBonus limitedBonus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPromoStartDate() {
		return promoStartDate;
	}

	public void setPromoStartDate(String promoStartDate) {
		this.promoStartDate = promoStartDate;
	}

	public String getPromoEndDate() {
		return promoEndDate;
	}

	public void setPromoEndDate(String promoEndDate) {
		this.promoEndDate = promoEndDate;
	}

	public int getPromoUsage() {
		return promoUsage;
	}

	public void setPromoUsage(int promoUsage) {
		this.promoUsage = promoUsage;
	}

	public int getCriteriaId() {
		return criteriaId;
	}

	public void setCriteriaId(int criteriaId) {
		this.criteriaId = criteriaId;
	}

	public int getBonusType() {
		return bonusType;
	}

	public void setBonusType(int bonusType) {
		this.bonusType = bonusType;
	}

	public double getBonusAmount() {
		return bonusAmount;
	}

	public void setBonusAmount(double bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	public double getMinDeposit() {
		return minDeposit;
	}

	public void setMinDeposit(double minDeposit) {
		this.minDeposit = minDeposit;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public double getMaxBonusAmount() {
		return maxBonusAmount;
	}

	public void setMaxBonusAmount(double maxBonusAmount) {
		this.maxBonusAmount = maxBonusAmount;
	}

	public int getChunks() {
		return chunks;
	}

	public void setChunks(int chunks) {
		this.chunks = chunks;
	}

	public double getWager() {
		return wager;
	}

	public void setWager(double wager) {
		this.wager = wager;
	}

	public int getBonusExpiryDays() {
		return bonusExpiryDays;
	}

	public void setBonusExpiryDays(int bonusExpiryDays) {
		this.bonusExpiryDays = bonusExpiryDays;
	}

	public int getPromoCodeStatus() {
		return promoCodeStatus;
	}

	public void setPromoCodeStatus(int promoCodeStatus) {
		this.promoCodeStatus = promoCodeStatus;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public int getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(int lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public String getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}


	public int getPromoType() {
		return promoType;
	}

	public void setPromoType(int promoType) {
		this.promoType = promoType;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getTermsConditionsLink() {
		return termsConditionsLink;
	}

	public void setTermsConditionsLink(String termsConditionsLink) {
		this.termsConditionsLink = termsConditionsLink;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public int getIsDisplayed() {
		return isDisplayed;
	}

	public void setIsDisplayed(int isDisplayed) {
		this.isDisplayed = isDisplayed;
	}

	public int getLimitedBonusType() {
		return limitedBonusType;
	}

	public void setLimitedBonusType(int limitedBonusType) {
		this.limitedBonusType = limitedBonusType;
	}

	public List<String> getUniquePromoCodes() {
		return uniquePromoCodes;
	}

	public void setUniquePromoCodes(List<String> uniquePromoCodes) {
		this.uniquePromoCodes = uniquePromoCodes;
	}

	public campaignPromo getCampaignPromo() {
		return campaignPromo;
	}

	public void setCampaignPromo(campaignPromo campaignPromo) {
		this.campaignPromo = campaignPromo;
	}

	public limitedBonus getLimitedBonus() {
		return limitedBonus;
	}

	public void setLimitedBonus(limitedBonus limitedBonus) {
		this.limitedBonus = limitedBonus;
	}

	@Override
	public String toString() {
		return "createPromoCodeServiceClass [id=" + id + ", promoType="
				+ promoType + ", promoCode=" + promoCode + ", promoStartDate="
				+ promoStartDate + ", promoEndDate=" + promoEndDate
				+ ", promoUsage=" + promoUsage + ", criteriaId=" + criteriaId
				+ ", bonusType=" + bonusType + ", bonusAmount=" + bonusAmount
				+ ", minDeposit=" + minDeposit + ", percentage=" + percentage
				+ ", maxBonusAmount=" + maxBonusAmount + ", chunks=" + chunks
				+ ", wager=" + wager + ", bonusExpiryDays=" + bonusExpiryDays
				+ ", promoCodeStatus=" + promoCodeStatus + ", createdBy="
				+ createdBy + ", creationDate=" + creationDate
				+ ", lastUpdateBy=" + lastUpdateBy + ", lastUpdateDate="
				+ lastUpdateDate + ", campaignId=" + campaignId
				+ ", termsConditionsLink=" + termsConditionsLink
				+ ", channelId=" + channelId + ", isDisplayed=" + isDisplayed
				+ ", limitedBonusType=" + limitedBonusType
				+ ", uniquePromoCodes=" + uniquePromoCodes + ", campaignPromo="
				+ campaignPromo + ", limitedBonus=" + limitedBonus + "]";
	}
}
