package com.rummycircle.rp;

public class RP_LPServiceClass {
	
	private long user_Id;
	private int monthly_lp;
	private int rp_balance;
	private int vip_lp;
	private String clubType;
	private String nextClubType;
	private int lpForNextClub;
	
	public long getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(long user_Id) {
		this.user_Id = user_Id;
	}
	public int getMonthly_lp() {
		return monthly_lp;
	}
	public void setMonthly_lp(int monthly_lp) {
		this.monthly_lp = monthly_lp;
	}
	public int getRp_balance() {
		return rp_balance;
	}
	public void setRp_balance(int rp_balance) {
		this.rp_balance = rp_balance;
	}
	public int getVip_lp() {
		return vip_lp;
	}
	public void setVip_lp(int vip_lp) {
		this.vip_lp = vip_lp;
	}
	public String getClubType() {
		return clubType;
	}
	public void setClubType(String clubType) {
		this.clubType = clubType;
	}
	public String getNextClubType() {
		return nextClubType;
	}
	public void setNextClubType(String nextClubType) {
		this.nextClubType = nextClubType;
	}
	public int getLpForNextClub() {
		return lpForNextClub;
	}
	public void setLpForNextClub(int lpForNextClub) {
		this.lpForNextClub = lpForNextClub;
	}
	@Override
	public String toString() {
		return "RP_LPServiceClass [user_Id=" + user_Id + ", monthly_lp="
				+ monthly_lp + ", rp_balance=" + rp_balance + ", vip_lp="
				+ vip_lp + ", clubType=" + clubType + ", nextClubType="
				+ nextClubType + ", lpForNextClub=" + lpForNextClub + "]";
	}
	
	

}