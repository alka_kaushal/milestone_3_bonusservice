package com.rummycircle.bonus;

import java.util.List;

public class BonusService {
	
	long userId;
	long promoCodeId;
	long price;
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getPromoCodeId() {
		return promoCodeId;
	}

	public void setPromoCodeId(long promoCodeId) {
		this.promoCodeId = promoCodeId;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	
	
	private List<BonusDetails>details;
	private List<RewardStoreBonusesDetails>StoreBonusdetails;
	
	
	

	

	public List<RewardStoreBonusesDetails> getStoreBonusdetails() {
		return StoreBonusdetails;
	}

	public void setStoreBonusdetails(
			List<RewardStoreBonusesDetails> storeBonusdetails) {
		StoreBonusdetails = storeBonusdetails;
	}

	public List<BonusDetails> getDetails() {
		return details;
	}

	public void setDetails(List<BonusDetails> details) {
		this.details = details;
	}

}
