package com.rummycircle.rp;


public class RPCreditServiceClass {
		private long userId;
		private long tournamentId;
		private long rewardPointValue;
		private double newrpSpendCounter;
		

		public double getNewrpSpendCounter() {
			return newrpSpendCounter;
		}

		public void setNewrpSpendCounter(double newrpSpendCounter) {
			this.newrpSpendCounter = newrpSpendCounter;
		}

		public long getUserId() {
		return userId;
		}

		public void setUserId(long userId) {
		this.userId = userId;
		}

		public long getTournamentId() {
		return tournamentId;
		}

		public void setTournamentId(long tournamentId) {
		this.tournamentId = tournamentId;
		}

		public long getRewardPointValue() {
		return rewardPointValue;
		}

		public void setRewardPointValue(long rewardPointValue) {
		this.rewardPointValue = rewardPointValue;
		}

		
		
		@Override
		public String toString() {
		return "RPCreditServiceClass [userId=" + userId + ", tournamentId=" + tournamentId + ", rewardPointValue="
		+ rewardPointValue + ", newrpSpendCounter="+ newrpSpendCounter + "]";
		}
}


