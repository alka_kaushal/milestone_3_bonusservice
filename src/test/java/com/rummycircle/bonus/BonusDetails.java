package com.rummycircle.bonus;

public class BonusDetails {
	
	private int id;
	private int promoId;
	private int promoType;
	private String promoCode;
	private int bonusType;
	private int totalChunks;
	private int chunksMoved;
	private int totalBonusAmount;
	private int bonusAmountPerChunk;
	private int bonusAmountReleased;
	private int totalEntryFee;
	private int totalEntryFeePlayed;
	private int entryFeePerChunk;
	private String bonusStartDate;
	private String bonusEndDate;
	private String bonusExpiryDate;
	private int status;
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getPromoId() {
		return promoId;
	}


	public void setPromoId(int promoId) {
		this.promoId = promoId;
	}


	public int getPromoType() {
		return promoType;
	}


	public void setPromoType(int promoType) {
		this.promoType = promoType;
	}


	public String getPromoCode() {
		return promoCode;
	}


	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}


	public int getBonusType() {
		return bonusType;
	}


	public void setBonusType(int bonusType) {
		this.bonusType = bonusType;
	}


	public int getTotalChunks() {
		return totalChunks;
	}


	public void setTotalChunks(int totalChunks) {
		this.totalChunks = totalChunks;
	}


	public int getChunksMoved() {
		return chunksMoved;
	}


	public void setChunksMoved(int chunksMoved) {
		this.chunksMoved = chunksMoved;
	}


	public int getTotalBonusAmount() {
		return totalBonusAmount;
	}


	public void setTotalBonusAmount(int totalBonusAmount) {
		this.totalBonusAmount = totalBonusAmount;
	}


	public int getBonusAmountPerChunk() {
		return bonusAmountPerChunk;
	}


	public void setBonusAmountPerChunk(int bonusAmountPerChunk) {
		this.bonusAmountPerChunk = bonusAmountPerChunk;
	}


	public int getBonusAmountReleased() {
		return bonusAmountReleased;
	}


	public void setBonusAmountReleased(int bonusAmountReleased) {
		this.bonusAmountReleased = bonusAmountReleased;
	}


	public int getTotalEntryFee() {
		return totalEntryFee;
	}


	public void setTotalEntryFee(int totalEntryFee) {
		this.totalEntryFee = totalEntryFee;
	}


	public int getTotalEntryFeePlayed() {
		return totalEntryFeePlayed;
	}


	public void setTotalEntryFeePlayed(int totalEntryFeePlayed) {
		this.totalEntryFeePlayed = totalEntryFeePlayed;
	}


	public int getEntryFeePerChunk() {
		return entryFeePerChunk;
	}


	public void setEntryFeePerChunk(int entryFeePerChunk) {
		this.entryFeePerChunk = entryFeePerChunk;
	}


	public String getBonusStartDate() {
		return bonusStartDate;
	}


	public void setBonusStartDate(String bonusStartDate) {
		this.bonusStartDate = bonusStartDate;
	}


	public String getBonusEndDate() {
		return bonusEndDate;
	}


	public void setBonusEndDate(String bonusEndDate) {
		this.bonusEndDate = bonusEndDate;
	}


	public String getBonusExpiryDate() {
		return bonusExpiryDate;
	}


	public void setBonusExpiryDate(String bonusExpiryDate) {
		this.bonusExpiryDate = bonusExpiryDate;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	
	
	
	@Override
	public String toString() {
		return "BonusService [id=" + id + ", promoId=" + promoId
				+ ", promoType=" + promoType + ", promoCode=" + promoCode
				+ ", bonusType=" + bonusType + ", totalChunks=" + totalChunks
				+ ", chunksMoved=" + chunksMoved + ", totalBonusAmount="
				+ totalBonusAmount + ", bonusAmountPerChunk="
				+ bonusAmountPerChunk + ", bonusAmountReleased="
				+ bonusAmountReleased + ", totalEntryFee=" + totalEntryFee
				+ ", totalEntryFeePlayed=" + totalEntryFeePlayed
				+ ", entryFeePerChunk=" + entryFeePerChunk
				+ ", bonusStartDate=" + bonusStartDate + ", bonusEndDate="
				+ bonusEndDate + ", bonusExpiryDate=" + bonusExpiryDate
				+ ", status=" + status + "]";
	}
	
	
	
	
	
	
	
	

}
