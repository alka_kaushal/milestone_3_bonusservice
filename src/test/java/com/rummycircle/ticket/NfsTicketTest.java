package com.rummycircle.ticket;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.mysql.fabric.xmlrpc.base.Array;
import com.rummycircle.NfsDao;
import com.rummycircle.ServicesEndPoint;
import com.rummycircle.restclient.HTTPHeaders;
import com.rummycircle.restclient.HTTPMethod;
import com.rummycircle.restclient.HTTPParams;
import com.rummycircle.restclient.HTTPRequest;
import com.rummycircle.restclient.HTTPResponse;
import com.rummycircle.utils.database.DatabaseManager;
import com.rummycircle.utils.exceptions.RCException;
import com.rummycircle.utils.testutils.BaseTest;
import com.rummycircle.utils.testutils.PropertyReader;
import com.rummycircle.utils.testutils.TestUtil;

public class NfsTicketTest extends BaseTest {

	private static Logger Log = Logger.getLogger(NfsTicketTest.class);
	Properties prop = PropertyReader.loadCustomProperties("custom.properties");

	private NfsDao nfsdao;

	public NfsTicketTest() {
		nfsdao = new NfsDao();
	}

	// GET: Ticket information using id (functional code)
	@Test
	public void getTicketDetailsByID() {

		String tick = prop.getProperty("TICKETID");
		int ticketid = Integer.parseInt(tick);

		String path = ServicesEndPoint.GET_TICKET_INFO_BY_ID + "/" + ticketid;
		System.out.println("path is " + path);

		Object str = null;
		HTTPRequest requestSpec1 = new HTTPRequest(str);

		HTTPResponse resp1 = rc.sendRequest(HTTPMethod.GET, path, requestSpec1);
		System.out.println("RESPONSE: " + resp1.getBody().getBodyText());
		System.out.println("RESPONSE STATUS: " + resp1.getStatusCode());
		TicketService rs = deserializeJsonToJavaObject(resp1.getBody()
				.getBodyText(), TicketService.class);

		try {

			List<List<Object>> items = nfsdao.getTicketDetailsByID();

			for (int j = 0; j < items.size(); j++) {

				String ExpiryDate = ((String) items.get(j).get(5));
				String CreatedOnDate = ((String) items.get(j).get(9));
				String UpdatedOn = ((String) items.get(j).get(10));
				System.out.println("UpdatedOn " + UpdatedOn);
				System.out.println("UpdatedOn after substring "
						+ ((String) items.get(j).get(10)).substring(0, 19));

				Assert.assertEquals(items.get(j).get(0), rs.getId());
				Assert.assertEquals(items.get(j).get(1), rs.getName());
				Assert.assertEquals(items.get(j).get(2), rs.getTicketValue());
				Assert.assertEquals(items.get(j).get(3), rs.getTicketType());
				Assert.assertEquals(items.get(j).get(4), rs.getExpiryType());
				if (ExpiryDate == null) {
					Assert.assertEquals(items.get(j).get(5), rs.getExpiryDate());
				} else {
					Assert.assertEquals(
							((String) items.get(j).get(5)).substring(0, 19),
							rs.getExpiryDate());
				}

				Assert.assertEquals(items.get(j).get(6), rs.isDisplayToUsers());
				Assert.assertEquals(items.get(j).get(7), rs.getTicketStatus());
				Assert.assertEquals(items.get(j).get(8), rs.getCreatedBy());

				if (CreatedOnDate == null) {
					Assert.assertEquals(items.get(j).get(9), rs.getCreatedOn());
				} else {
					Assert.assertEquals(
							((String) items.get(j).get(9)).substring(0, 19),
							rs.getCreatedOn());
				}

				if (UpdatedOn == null) {
					Assert.assertEquals(items.get(j).get(10),
							rs.getUpdateDate());
				} else {
					Assert.assertEquals(
							((String) items.get(j).get(10)).substring(0, 19),
							rs.getUpdateDate());
				}

			}

		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}// end of get ticket details by id

	// GET: returns the list of all available tickets with details
	@Test
	public void TicketList() {

		String path = ServicesEndPoint.GET_TICKET_LIST + "list";
		TicketService service = new TicketService();

		Object str = null;
		HTTPRequest request = new HTTPRequest(str);
		System.out.println("request is  " + request.toString());

		HTTPResponse response = rc.sendRequest(HTTPMethod.GET, path, request);
		System.out.println("RESPONSE " + response.getBody().getBodyText());
		System.out.println("RESPONSE STATUS" + response.getStatusCode());

		// response is an array of json type and each element in array is
		// hashmap
		List<HashMap> details = JsonPath
				.parse(response.getBody().getBodyText()).read("$.[*]");
		int countDb = nfsdao.getCount();
		System.out.println("count from DB " + countDb);
		long responseSize = details.size();

		// checking if the No. of elements in response = No of elements in db
		Assert.assertEquals(countDb, responseSize);

		List<List<Object>> items = nfsdao.getTicketDetails();

		for (int j = 0; j < details.size(); j++) {
			HashMap map = details.get(j);

			String ExpiryDate = ((String) items.get(j).get(5));
			String CreatedOnDate = ((String) items.get(j).get(9));
			String UpdatedOn = ((String) items.get(j).get(10));

			Assert.assertEquals(items.get(j).get(0), map.get("id"));
			Assert.assertEquals(items.get(j).get(1), map.get("name"));
			// Assert.assertEquals( items.get(j).get(2),map.get("ticketValue"));
			Assert.assertEquals(items.get(j).get(3), map.get("ticketType"));
			Assert.assertEquals(items.get(j).get(4), map.get("expiryType"));
			if (ExpiryDate == null) {
				Assert.assertEquals(items.get(j).get(5), map.get("expiryDate"));
			} else {
				Assert.assertEquals(
						((String) items.get(j).get(5)).substring(0, 19),
						map.get("expiryDate"));
			}

			Assert.assertEquals(items.get(j).get(6), map.get("displayToUsers"));
			Assert.assertEquals(items.get(j).get(7), map.get("ticketStatus"));
			Assert.assertEquals(items.get(j).get(8), map.get("createdBy"));

			if (CreatedOnDate == null) {
				Assert.assertEquals(items.get(j).get(9), map.get("createdOn"));
			} else {
				Assert.assertEquals(
						((String) items.get(j).get(9)).substring(0, 19),
						map.get("createdOn"));
			}

			if (UpdatedOn == null) {
				Assert.assertEquals(items.get(j).get(10), map.get("updateDate"));
			} else {
				Assert.assertEquals(
						((String) items.get(j).get(10)).substring(0, 19),
						map.get("updateDate"));
			}

		}

		try {
		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}// eof ticketlist

	// POST : For creating ticket with provided details
	@Test
	public void CreateTicket() {
		String path = ServicesEndPoint.CREATE_TICKET;
		TicketService ss = new TicketService();
		System.out.println("path is " + path);

		String name = prop.getProperty("CreateTicket_NAME");

		String tickval = prop.getProperty("CreateTicket_TICKETVALUE");
		int ticketValue = Integer.parseInt(tickval);

		String ticktype = prop.getProperty("CreateTicket_TICKETTYE");
		int ticketType = Integer.parseInt(ticktype);

		String expType = prop.getProperty("CreateTicket_EXPIRYTYPE");
		int expiryType = Integer.parseInt(expType);

		String expiryDate = prop.getProperty("CreateTicket_EXPIRYDATE");
		System.out.println(" exp date " + expiryDate);
		String dispToUsr = prop.getProperty("CreateTicket_DISPLAYTOUSERS");
		boolean displayToUsers = Boolean.parseBoolean(dispToUsr);

		String createdBy = prop.getProperty("CreateTicket_CREATEDBY");

		ss.setName(name);
		ss.setTicketValue(ticketValue);
		ss.setTicketType(ticketType);
		ss.setExpiryType(expiryType);
		ss.setExpiryDate(expiryDate);
		ss.setDisplayToUsers(displayToUsers);
		ss.setCreatedBy(createdBy);

		HTTPRequest request = new HTTPRequest(ss);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

		TicketService rs = deserializeJsonToJavaObject(httpResp.getBody()
				.getBodyText(), TicketService.class);

		System.out.println("RESPONSE : " + httpResp.getBody().getBodyText());
		System.out.println("RESPONSE STATUS: " + httpResp.getStatusCode());

		try {

			List<List<Object>> items = nfsdao.getTickDetailsAfterCreate(rs
					.getId());

			for (int j = 0; j < items.size(); j++) {

				String ExpiryDate = ((String) items.get(j).get(5));
				String CreatedOnDate = ((String) items.get(j).get(9));
				String UpdatedOn = ((String) items.get(j).get(10));

				Assert.assertEquals(items.get(j).get(0), rs.getId());
				Assert.assertEquals(items.get(j).get(1), rs.getName());
				Assert.assertEquals(items.get(j).get(2), rs.getTicketValue());
				Assert.assertEquals(items.get(j).get(3), rs.getTicketType());
				Assert.assertEquals(items.get(j).get(4), rs.getExpiryType());
				/*
				 * if (ExpiryDate == null) {
				 * Assert.assertEquals(items.get(j).get(5), rs.getExpiryDate());
				 * } else { Assert.assertEquals(((String) items.get(j).get(5))
				 * .substring(0, 16), rs.getExpiryDate().substring(0, 16)); }
				 */

				Assert.assertEquals(items.get(j).get(6), rs.isDisplayToUsers());
				Assert.assertEquals(items.get(j).get(7), rs.getTicketStatus());
				Assert.assertEquals(items.get(j).get(8), rs.getCreatedBy());

				/*
				 * if (CreatedOnDate == null) {
				 * Assert.assertEquals(items.get(j).get(9), rs.getCreatedOn());
				 * } else { Assert.assertEquals(((String) items.get(j).get(9))
				 * .substring(0, 16), rs.getCreatedOn().substring(0, 16)); }
				 * 
				 * if (UpdatedOn == null) {
				 * Assert.assertEquals(items.get(j).get(10),
				 * rs.getUpdateDate()); } else { Assert.assertEquals( ((String)
				 * items.get(j).get(10)).substring(0, 16),
				 * rs.getUpdateDate().substring(0, 16)); }
				 */

			}
		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	} // eof create test

	// POST : For updating ticket with provided details
	@Test
	public void updateticket() throws ParseException {

		TicketService ss = new TicketService();

		int ticketid = Integer.parseInt(prop.getProperty("UPDATE_TICKETID"));
		String name = prop.getProperty("UDATETICKET_NAME");
		int ticketValue = Integer.parseInt(prop
				.getProperty("UDATETICKET_TICKETVALUE"));
		int ticketType = Integer.parseInt(prop
				.getProperty("UDATETICKET_TICKETTYPE"));
		int expiryType = Integer.parseInt(prop
				.getProperty("UDATETICKET_EXPIRYTYPE"));
		String expiryDate = prop.getProperty("UDATETICKET_EXPIRYDATE");
		boolean displayToUsers = Boolean.parseBoolean(prop
				.getProperty("UDATETICKET_DISPLAYTOUSERS"));
		String createdBy = prop.getProperty("UDATETICKET_CREATEDBY");

		List<List<Object>> BeforeUpdate = nfsdao
				.getTickDetailsAfterCreate(ticketid);

		ss.setName(name);
		ss.setTicketValue(ticketValue);
		ss.setTicketType(ticketType);
		ss.setExpiryType(expiryType);
		ss.setExpiryDate(expiryDate);
		ss.setDisplayToUsers(displayToUsers);
		ss.setCreatedBy(createdBy);

		String path = ServicesEndPoint.UPDATE_TICKET + "/" + ticketid;
		System.out.println("path is " + path);
		HTTPRequest request = new HTTPRequest(ss);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

		TicketService rs = deserializeJsonToJavaObject(httpResp.getBody()
				.getBodyText(), TicketService.class);

		System.out.println("response status is :" + httpResp.getStatusCode());

		try {
			// checking the details of updated ticketid from DB after update
			// request
			List<List<Object>> items = nfsdao
					.getTickDetailsAfterCreate(ticketid);
			for (int j = 0; j < items.size(); j++) {

				// checking if the value in response and DB is same
				String ExpiryDate = ((String) items.get(j).get(5));
				String CreatedOnDate = ((String) items.get(j).get(9));
				String UpdatedOn = ((String) items.get(j).get(10));

				Assert.assertEquals(items.get(j).get(0), rs.getId());
				Assert.assertEquals(items.get(j).get(1), rs.getName());
				Assert.assertEquals(items.get(j).get(2), rs.getTicketValue());
				Assert.assertEquals(items.get(j).get(3), rs.getTicketType());
				Assert.assertEquals(items.get(j).get(4), rs.getExpiryType());
				/*
				 * if (ExpiryDate == null) {
				 * Assert.assertEquals(items.get(j).get(5), rs.getExpiryDate());
				 * } else { Assert.assertEquals( ((String)
				 * items.get(j).get(5)).substring(0, 19), rs.getExpiryDate()); }
				 */
				Assert.assertEquals(items.get(j).get(6), rs.isDisplayToUsers());
				Assert.assertEquals(items.get(j).get(7), rs.getTicketStatus());
				Assert.assertEquals(items.get(j).get(8), rs.getCreatedBy());

				/*
				 * if (CreatedOnDate == null) {
				 * Assert.assertEquals(items.get(j).get(9), rs.getCreatedOn());
				 * } else { Assert.assertEquals( ((String)
				 * items.get(j).get(9)).substring(0, 19), rs.getCreatedOn()); }
				 */

				/*
				 * if (UpdatedOn == null) {
				 * Assert.assertEquals(items.get(j).get(10),
				 * rs.getUpdateDate()); } else { Assert.assertEquals( ((String)
				 * items.get(j).get(10)).substring(0, 19), rs.getUpdateDate());
				 * }
				 */

			}

		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}// End of updateticket

	// POST: Assigns tickets to list of players
	@Test
	public void AssignTicket() {

		String path = ServicesEndPoint.ASSIGN_TO_USERS;

		TicketServiceForList service = new TicketServiceForList();

		int ticketId = Integer.parseInt(prop.getProperty("ASSIGN_TICKETID"));
		int ticketId2 = Integer.parseInt(prop.getProperty("ASSIGN_TICKETID2"));
		int noOfTickets = Integer.parseInt(prop
				.getProperty("ASSIGN_NOOFTICKETS"));
		int noOfTickets2 = Integer.parseInt(prop
				.getProperty("ASSIGN_NOOFTICKETS2"));
		String comments = prop.getProperty("ASSIGN_COMMENTS");
		String comments2 = prop.getProperty("ASSIGN_COMMENTS2");
		String ticketType = prop.getProperty("ASSIGN_TICKETTYPE");
		String ticketType2 = prop.getProperty("ASSIGN_TICKETTYPE2");
		String endDate = prop.getProperty("ASSIGN_ENDDATE");
		String endDate2 = prop.getProperty("ASSIGN_ENDDATE2");
		long userIds[] = new long[] {
				Long.parseLong(prop.getProperty("ASSIGN_UID1")),
				Long.parseLong(prop.getProperty("ASSIGN_UID2")) };
		long userIds2[] = new long[] {
				Long.parseLong(prop.getProperty("ASSIGN22_UID1")),
				Long.parseLong(prop.getProperty("ASSIGN22_UID2")) };

		// do not give end date in fixed type ticket
		Details ad = new Details();
		ad.setTicketId(ticketId);
		ad.setNoOfTickets(noOfTickets);
		ad.setComments(comments);
		ad.setTicketType(ticketType);
		// ad.setEndDate(endDate);
		ad.setUserIds(userIds);

		Details ad2 = new Details();

		ad2.setTicketId(ticketId2);
		ad2.setNoOfTickets(noOfTickets2);
		ad2.setComments(comments2);
		ad2.setTicketType(ticketType2);
		ad2.setEndDate(endDate2);
		ad2.setUserIds(userIds2);

		List<Details> details = new ArrayList<Details>();

		details.add(ad);
		details.add(ad2);
		service.setDetails(details);

		HTTPRequest request = new HTTPRequest(service);

		System.out.println("request is " + details);

		HTTPResponse resp = rc.sendRequest(HTTPMethod.POST, path, request);

		System.out.println(" response status is  " + resp.getStatusCode());

		try {

		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}

	// NFS milestone 3 code
	// POST method :use a ticket
	/**
	 * If ticket status was changed in DB then this method will return
	 * update=true
	 */

	

	@Test
	public void testUseTicket() {

		// Functional code

		String uid = prop.getProperty("USE_USERID");
		int userID = Integer.parseInt(uid);

		String tick = prop.getProperty("USE_TICKETID");
		int ticketID = Integer.parseInt(tick);

		String tid = prop.getProperty("USE_TOURNAMENTID");
		int tournamentID = Integer.parseInt(tid);

		TicketServicePSP ss = new TicketServicePSP();
		ss.setUserID(userID);
		ss.setTournamentID(tournamentID);
		ss.setTicketID(ticketID);
		//
		String path = ServicesEndPoint.USE_A_TICKET + userID + "/use/"
				+ ticketID + "/" + tournamentID;
		System.out.println("path is " + path);

		HTTPRequest request = new HTTPRequest(ss);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

		TicketServicePSP rs = deserializeJsonToJavaObject(httpResp.getBody()
				.getBodyText(), TicketServicePSP.class);

		List<List<Object>> items = nfsdao.getUseTicket(ticketID, userID);
		for (int j = 0; j < items.size(); j++) {
			System.out.println("Status from DB is " + items.get(j));
		}

		System.out.println("response status is :" + httpResp.getStatusCode());
		System.out.println("RESPONSE " + httpResp.getBody().getBodyText());

		// ENd of functional code

	}

	// POST method: reset ticket
	/** This method changes the status of ticket in DB */
	@Test
	public void testResetTicket() {
		String uid = prop.getProperty("RESET_USERID");
		int userID = Integer.parseInt(uid);

		String tid = prop.getProperty("RESET_TOURNAMENTID");
		int tournamentID = Integer.parseInt(tid);

		TicketServicePSP ss = new TicketServicePSP();
		ss.setUserID(userID);
		ss.setTournamentID(tournamentID);

		List<List<Object>> BeforeReset = nfsdao.getResetTicket(tournamentID,
				userID);
		for (int j = 0; j < BeforeReset.size(); j++) {
			System.out.println("Status from DB is before reset "
					+ BeforeReset.get(j));
		}

		String path = ServicesEndPoint.RESET_TICKET + userID + "/reset" + "/"
				+ tournamentID;
		System.out.println("path is " + path);

		HTTPRequest request = new HTTPRequest(ss);

		HTTPResponse httpResp = rc.sendRequest(HTTPMethod.POST, path, request);

		TicketServicePSP rs = deserializeJsonToJavaObject(httpResp.getBody()
				.getBodyText(), TicketServicePSP.class);

		List<List<Object>> items = nfsdao.getResetTicket(tournamentID, userID);
		for (int j = 0; j < items.size(); j++) {
			System.out.println("Status from DB after reset is " + items.get(j));
		}

		System.out.println("response status is :" + httpResp.getStatusCode());
		System.out.println("RESPONSE " + httpResp.getBody().getBodyText());

	}

	// GET method: get user tickets
	@Test
	/** This method returns the list of tickets assigned to specific user when display to users= */
	public void testGetUserTicketsDisplayTousers() {
		// http://10.14.24.76:8080/nfs/api/ticketUser/360545/getTickets/
		String uid = prop.getProperty("UserTick_USERID");
		int userID = Integer.parseInt(uid);

		String disp = prop.getProperty("UserTick_DISPLAYTOUSERS");

		String path = ServicesEndPoint.GET_USER_TICKET + userID
				+ "/getTickets/?displayToUsers=" + disp;
		System.out.println("path is " + path);

		Object str = null;
		HTTPRequest request = new HTTPRequest(str);
		System.out.println("request is  " + request.toString());

		HTTPResponse response = rc.sendRequest(HTTPMethod.GET, path, request);
		System.out.println("RESPONSE " + response.getBody().getBodyText());
		System.out.println("RESPONSE STATUS" + response.getStatusCode());

		// response is an array of json type and each element in array is
		// hashmap
		List<HashMap> details = JsonPath
				.parse(response.getBody().getBodyText()).read("$.[*]");

		List<List<Object>> items2 = nfsdao.getViewUserTicketsDisplayToUser(
				userID, disp);

		System.out.println("count of tickets " + items2.size());
		for (int j = 0; j < items2.size(); j++) {
			HashMap map = details.get(j);
			Assert.assertEquals(items2.get(j).get(0), map.get("id"));
			Assert.assertEquals(items2.get(j).get(1), map.get("ticketId"));
			Assert.assertEquals(items2.get(j).get(2), map.get("ticketName"));
			Assert.assertEquals(items2.get(j).get(3), map.get("ticketType"));
			Assert.assertEquals(items2.get(j).get(5), map.get("status"));
			Assert.assertEquals(items2.get(j).get(6), map.get("comments"));
			Assert.assertEquals(items2.get(j).get(9), map.get("tournamentId"));

			/*
			 * System.out.println("Id: " + map.get("id")+
			 * " : "+items2.get(j).get(0)); System.out.println("ticketId: " +
			 * map.get("ticketId")+ " : "+items2.get(j).get(1));
			 * System.out.println("ticketName: " + map.get("ticketName")+
			 * " : "+items2.get(j).get(2)); System.out.println("ticketType: " +
			 * map.get("ticketType")+ " : "+items2.get(j).get(3));
			 * System.out.println("ticketExpiryDate: " +
			 * map.get("ticketExpiryDate")+ " : "+items2.get(j).get(4));
			 * System.out.println("userId: " + map.get("userId"));
			 * System.out.println("status: " + map.get("status")+
			 * " : "+items2.get(j).get(5)); System.out.println("comments: " +
			 * map.get("comments")+ " : "+items2.get(j).get(6));
			 * System.out.println("assignDate: " + map.get("assignDate")+
			 * " : "+items2.get(j).get(7)); System.out.println("expiryDate: " +
			 * map.get("expiryDate")+ " : "+items2.get(j).get(8));
			 * System.out.println("tournamentId: " + map.get("tournamentId")+
			 * " : "+items2.get(j).get(9)); System.out.println("lastUsed: " +
			 * map.get("lastUsed")+ " : "+items2.get(j).get(10));
			 */
		}

		try {
		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}

	@Test
	/** This method returns the list of tickets assigned to specific user when status=  */
	public void testGetUserTicketsStatus() {

		// http://10.14.24.76:8080/nfs/api/ticketUser/360545/getTickets/
		String uid = prop.getProperty("UserTick_USERID");
		int userID = Integer.parseInt(uid);

		String stat = prop.getProperty("UserTick_STATUS");
		int status = Integer.parseInt(stat);

		String path = ServicesEndPoint.GET_USER_TICKET + userID
				+ "/getTickets/?status=" + status;
		System.out.println("path is " + path);

		Object str = null;
		HTTPRequest request = new HTTPRequest(str);
		System.out.println("request is  " + request.toString());

		HTTPResponse response = rc.sendRequest(HTTPMethod.GET, path, request);
		System.out.println("RESPONSE " + response.getBody().getBodyText());
		System.out.println("RESPONSE STATUS" + response.getStatusCode());

		// response is an array of json type and each element in array is
		// hashmap
		List<HashMap> details = JsonPath
				.parse(response.getBody().getBodyText()).read("$.[*]");

		List<List<Object>> items2 = nfsdao.getViewUserTicketsStatus(userID,
				status);
		System.out.println("count of records " + items2.size());

		for (int j = 0; j < items2.size(); j++) {
			HashMap map = details.get(j);
			System.out.println("Id: " + map.get("id") + " : "
					+ items2.get(j).get(0));
			System.out.println("ticketId: " + map.get("ticketId") + " : "
					+ items2.get(j).get(1));
			System.out.println("ticketName: " + map.get("ticketName") + " : "
					+ items2.get(j).get(2));
			System.out.println("ticketType: " + map.get("ticketType") + " : "
					+ items2.get(j).get(3));
			System.out.println("ticketExpiryDate: "
					+ map.get("ticketExpiryDate") + " : "
					+ items2.get(j).get(4));
			System.out.println("userId: " + map.get("userId"));
			System.out.println("status: " + map.get("status") + " : "
					+ items2.get(j).get(5));
			System.out.println("comments: " + map.get("comments") + " : "
					+ items2.get(j).get(6));
			System.out.println("assignDate: " + map.get("assignDate") + " : "
					+ items2.get(j).get(7));
			System.out.println("expiryDate: " + map.get("expiryDate") + " : "
					+ items2.get(j).get(8));
			System.out.println("tournamentId: " + map.get("tournamentId")
					+ " : " + items2.get(j).get(9));
			System.out.println("lastUsed: " + map.get("lastUsed") + " : "
					+ items2.get(j).get(10));

			/*
			 * System.out.println("Id: " + map.get("id") +
			 * " : "+items.get(j).get(0)); System.out.println("Name: " +
			 * map.get("name") + " : "+ items.get(j).get(1));
			 * System.out.println("ticket value: " + map.get("ticketValue")+
			 * " : " + items.get(j).get(2)); System.out.println("ticket type: "
			 * + map.get("ticketType") + " : "+items.get(j).get(3));
			 * System.out.println("expiry type: " + map.get("expiryType") +
			 * " : "+ items.get(j).get(4)); try {
			 * System.out.println("expiry date: " + map.get("expiryDate")+ " : "
			 * + items.get(j).get(5)); } catch (Exception e) {
			 * e.printStackTrace(); }
			 * 
			 * System.out.println("display to users: " +
			 * map.get("displayToUsers")+ " : " +items.get(j).get(6));
			 * System.out.println("ticket status: " + map.get("ticketStatus") +
			 * " : " + items.get(j).get(7) ); System.out.println("Created by:  "
			 * + map.get("createdBy") + " : "+ items.get(j).get(8));
			 * 
			 * try { System.out.println("Created on:  " + map.get("createdOn")+
			 * " : " + items.get(j).get(9)); } catch (Exception e) {
			 * e.printStackTrace(); }
			 * 
			 * System.out.println("Update date :  " + map.get("updateDate") +
			 * " : "+(String) items.get(j).get(10));
			 */
		}

		try {
		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}

	@Test
	/** This method returns the list of tickets assigned to specific user when limit=  */
	public void testGetUserTicketslimit() {

		// http://10.14.24.76:8080/nfs/api/ticketUser/360545/getTickets/
		String uid = prop.getProperty("UserTick_USERID");
		int userID = Integer.parseInt(uid);

		String lim = prop.getProperty("UserTick_LIMIT");
		int limit = Integer.parseInt(lim);

		String path = ServicesEndPoint.GET_USER_TICKET + userID
				+ "/getTickets/?limit=" + limit;
		System.out.println("path is " + path);

		Object str = null;
		HTTPRequest request = new HTTPRequest(str);
		System.out.println("request is  " + request.toString());

		HTTPResponse response = rc.sendRequest(HTTPMethod.GET, path, request);
		System.out.println("RESPONSE " + response.getBody().getBodyText());
		System.out.println("RESPONSE STATUS" + response.getStatusCode());

		// response is an array of json type and each element in array is
		// hashmap
		List<HashMap> details = JsonPath
				.parse(response.getBody().getBodyText()).read("$.[*]");

		List<List<Object>> items2 = nfsdao.getViewUserTicketsLimit(userID,
				limit);
		System.out.println("count of records " + items2.size());

		for (int j = 0; j < items2.size(); j++) {
			HashMap map = details.get(j);
			System.out.println("Id: " + map.get("id") + " : "
					+ items2.get(j).get(0));
			System.out.println("ticketId: " + map.get("ticketId") + " : "
					+ items2.get(j).get(1));
			System.out.println("ticketName: " + map.get("ticketName") + " : "
					+ items2.get(j).get(2));
			System.out.println("ticketType: " + map.get("ticketType") + " : "
					+ items2.get(j).get(3));
			System.out.println("ticketExpiryDate: "
					+ map.get("ticketExpiryDate") + " : "
					+ items2.get(j).get(4));
			System.out.println("userId: " + map.get("userId"));
			System.out.println("status: " + map.get("status") + " : "
					+ items2.get(j).get(5));
			System.out.println("comments: " + map.get("comments") + " : "
					+ items2.get(j).get(6));
			System.out.println("assignDate: " + map.get("assignDate") + " : "
					+ items2.get(j).get(7));
			System.out.println("expiryDate: " + map.get("expiryDate") + " : "
					+ items2.get(j).get(8));
			System.out.println("tournamentId: " + map.get("tournamentId")
					+ " : " + items2.get(j).get(9));
			System.out.println("lastUsed: " + map.get("lastUsed") + " : "
					+ items2.get(j).get(10));

			/*
			 * System.out.println("Id: " + map.get("id") +
			 * " : "+items.get(j).get(0)); System.out.println("Name: " +
			 * map.get("name") + " : "+ items.get(j).get(1));
			 * System.out.println("ticket value: " + map.get("ticketValue")+
			 * " : " + items.get(j).get(2)); System.out.println("ticket type: "
			 * + map.get("ticketType") + " : "+items.get(j).get(3));
			 * System.out.println("expiry type: " + map.get("expiryType") +
			 * " : "+ items.get(j).get(4)); try {
			 * System.out.println("expiry date: " + map.get("expiryDate")+ " : "
			 * + items.get(j).get(5)); } catch (Exception e) {
			 * e.printStackTrace(); }
			 * 
			 * System.out.println("display to users: " +
			 * map.get("displayToUsers")+ " : " +items.get(j).get(6));
			 * System.out.println("ticket status: " + map.get("ticketStatus") +
			 * " : " + items.get(j).get(7) ); System.out.println("Created by:  "
			 * + map.get("createdBy") + " : "+ items.get(j).get(8));
			 * 
			 * try { System.out.println("Created on:  " + map.get("createdOn")+
			 * " : " + items.get(j).get(9)); } catch (Exception e) {
			 * e.printStackTrace(); }
			 * 
			 * System.out.println("Update date :  " + map.get("updateDate") +
			 * " : "+(String) items.get(j).get(10));
			 */
		}

		try {
		} catch (RCException e) {
			System.out.println("in exception");
			Log.error("Error :: RESPONSE IS EMPTY" + e.getMessage());
		}

	}

	public void ajTesting() {

	}

}
