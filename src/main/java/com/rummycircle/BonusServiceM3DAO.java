package com.rummycircle;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.rummycircle.utils.database.AbstractDAO;
import com.rummycircle.utils.testutils.PropertyReader;

public class BonusServiceM3DAO extends AbstractDAO {

	private JdbcTemplate jdbcTemplate;

	Properties prop = PropertyReader.loadCustomProperties("custom.properties");

	public BonusServiceM3DAO() {

		this.jdbcTemplate = new JdbcTemplate(getDataSource());

	}

	public List<List<Object>> getPromoCodes(int promo_type) {
		String query = null;
		if (promo_type == 0) {
			query = String.format("select * from bonus_promotions");
		} else {
			query = String
					.format("select * from bonus_promotions where promo_type = "
							+ promo_type);
		}
		try {
			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	public int getCountPromoCode(int promo_type) {
		int count;
		String query = null;
		if (promo_type == 0) {
			query = String.format("select count(*) from bonus_promotions");
		} else {
			query = String
					.format("select count(*) from bonus_promotions where promo_type ="
							+ promo_type);
		}
		try {
			count = jdbcTemplate.queryForObject(query, Integer.class);

		} catch (EmptyResultDataAccessException e) {
			return 0;
		}

		return count;

	}


	public List<List<Object>> getBonusPromotionsAllOptions(int intDateType,
			String fromDate, String toDate, int status, int createdBy,
			int promo_type) {
		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	// Done
	public List<List<Object>> getPromoBonusPromotionsStatusCreatedBy(
			int intDateType, String fromDate, String toDate, int status,
			int createdBy) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by=" + createdBy);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by=" + createdBy);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by=" + createdBy);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and created_by=" + createdBy);

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}
// Done
	public List<List<Object>> getPromoBonusPromotionsfromStatusPromoType(
			int intDateType, String fromDate, String toDate, int status,
			int promo_type) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and promo_type=" + promo_type);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and promo_type=" + promo_type);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and promo_type=" + promo_type);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status
							+ " and promo_type=" + promo_type);

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}
// Done
	public List<List<Object>> getPromoBonusPromotionsFromStatus(int intDateType,String fromDate,
			String toDate, int status) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_code_status="
							+ status);

		}
		
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	
	public List<List<Object>> getPromoBonusPromotionsfromPromoTypeCreatedBy(
			int intDateType, String fromDate, String toDate, int createdBy,
			int promo_type) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy
							+ " and promo_type=" + promo_type);

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}
	// Done
	public List<List<Object>> getPromoBonusPromotionsfromCreatedBy(int intDateType,
			String fromDate, String toDate, int createdBy) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and created_by="
							+ createdBy);

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}
	
	// Done
	public List<List<Object>> getPromoBonusPromotionsfromPromoType(int intDateType,
			String fromDate, String toDate, int IntpromoType) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_type="
							+ IntpromoType);

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_type="
							+ IntpromoType);

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_type="
							+ IntpromoType);
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\' and promo_type="
							+ IntpromoType);

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	
	public List<List<Object>> getPromoBonusPromotionsfromDateType(int intDateType,
			String fromDate, String toDate) {

		String query = null;

		if (intDateType == 1) {
			query = String
					.format("select * from bonus_promotions where creation_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\'");

		} else if (intDateType == 2) {
			query = String
					.format("select * from bonus_promotions where last_update_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\'");

		} else if (intDateType == 3) {
			query = String
					.format("select * from bonus_promotions where promo_start_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\'");
		} else {
			query = String
					.format("select * from bonus_promotions where promo_end_date between \'"
							+ fromDate
							+ "\'"
							+ " AND \'"
							+ toDate
							+ "\'");

		}

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	
	
	
	

	public List<List<Object>> getLimitedbonuspromotions(int bonus_promotion_id) {

		String query = String
				.format("select * from limited_bonus_promotions where bonus_promotion_id="
						+ bonus_promotion_id);
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("limited_quantity"));
					list.add(1, rs.getInt("display_hour_after"));
					list.add(2, rs.getInt("reserved_time"));
					list.add(3, rs.getInt("ticket_id"));
					list.add(4, rs.getInt("message_text"));
					list.add(5, rs.getInt("message_start_time"));
					return list;

				}
			});
		} catch (Exception e) {

			e.printStackTrace();
			return null;

		}
	}

	public List<List<Object>> getCampaignPromotions(int id) {

		String query = String
				.format("select * from campaign_promotions where id=" + id);
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("name"));
					list.add(1, rs.getInt("offer_type"));
					list.add(2, rs.getInt("status"));

					return list;

				}
			});
		} catch (Exception e) {

			e.printStackTrace();
			return null;

		}
	}

	public List<List<Object>> getSearchPromoCodes(int id) {

		String query = String.format("select * from bonus_promotions where id="
				+ id);
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("name"));

					return list;

				}
			});
		} catch (Exception e) {

			e.printStackTrace();
			return null;

		}

	}

	public List<List<Object>> getPromoBonusPromotions(String promo_code) {

		String query = String
				.format("select * from bonus_promotions where promo_code=\""
						+ promo_code+"\"");
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {

					List<Object> list = new ArrayList<Object>();

					list.add(0, rs.getInt("id"));
					list.add(1, rs.getInt("promo_type"));
					list.add(2, rs.getString("promo_code"));
					list.add(3, rs.getString("promo_start_date"));
					list.add(4, rs.getString("promo_end_date"));
					list.add(5, rs.getInt("promo_usage"));
					list.add(6, rs.getInt("criteria_id"));
					list.add(7, rs.getInt("bonus_type"));
					list.add(8, rs.getLong("bonus_amount"));
					list.add(9, rs.getLong("min_deposit"));
					list.add(10, rs.getLong("percentage"));
					list.add(11, rs.getLong("max_bonus_amount"));
					list.add(12, rs.getInt("chunks"));
					list.add(13, rs.getLong("wager"));
					list.add(14, rs.getInt("bonus_expiry_days"));
					list.add(15, rs.getInt("promo_code_status"));
					list.add(16, rs.getInt("created_by"));
					list.add(17, rs.getString("creation_date"));
					list.add(18, rs.getInt("last_update_by"));
					list.add(19, rs.getString("last_update_date"));
					list.add(20, rs.getInt("campaign_id"));
					list.add(21, rs.getString("terms_conditions_link"));
					list.add(22, rs.getInt("channel_id"));
					list.add(23, rs.getInt("is_displayed"));
					list.add(24, rs.getInt("limited_bonus_type"));
					return list;
				}
			});

		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

}
