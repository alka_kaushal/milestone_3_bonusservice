package com.rummycircle;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
 

import com.rummycircle.utils.testutils.PropertyReader;

import org.springframework.stereotype.Component;


import com.rummycircle.utils.database.AbstractDAO;
import com.rummycircle.utils.testutils.PropertyReader;

@Component

public class NfsDao extends AbstractDAO{

	private JdbcTemplate jdbcTemplate;

	Properties prop = PropertyReader.loadCustomProperties("custom.properties");
	String tick=prop.getProperty("TICKETID");
	int ticketid=Integer.parseInt(tick);



	public NfsDao() {

		this.jdbcTemplate = new JdbcTemplate(getDataSource());

	}

	public int getRPfromNFT(long userId) {
		Integer result= null;
		String query = String.format("select rp_balance from nft_player_accounts where user_id="+userId);
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class);


		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}

	public int getRPfromPlayer_RP(long userId) {
		Integer result= null;
		String query = String.format("select updated_balance from player_rp_transactions where user_id="+userId+" order by update_time desc limit 1");
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class); 

		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}

	public int getRPfromMonthly_EntryFee(long userId) {
		Integer result= null;
		
		java.util.Date date = new java.util.Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		int currentMonth = cal.get(Calendar.MONTH)+1; // 1 is added because the calculation of months starts from 0-Jan 1-Feb etc
		int currentYear = cal.get(Calendar.YEAR);
		
		String query = String.format("select reward_points from monthly_entry_fee_history where userid="+userId+" and month="+currentMonth+" and year= "+currentYear);
		try {
			result =  jdbcTemplate.queryForObject(query, Integer.class);


		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
		return result;
	}

	public List<List<Object>> getUserDataFromNFT(long user_id){

		String query=String.format("select lp_spend_credit_counter,monthly_lp,vip_lp,rp_balance,club_type from nft_player_accounts where user_id="+user_id);

		try {
			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("monthly_lp"));
					list.add(1, rs.getInt("vip_lp"));
					list.add(2, rs.getInt("rp_balance"));
					list.add(3, rs.getInt("club_type"));

					return list;
				}
			});
		} 
		catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	public List<List<Object>> getCLUB_LP(int nextClub_type){
		String query=String.format("select club_name,min_lp_required from club_reference where id="+nextClub_type);

		try {
			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getString("club_name"));
					list.add(1, rs.getInt("min_lp_required"));

					return list;
				}
			});
		} 
		catch (EmptyResultDataAccessException e) {
			return null;
		}

	}


	// DAO for Tickets


	public List<List<Object>> getTicketDetailsByID(){
		String query="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("id"));
					list.add(1, rs.getString("name"));
					list.add(2, rs.getInt("ticket_value"));
					list.add(3, rs.getInt("ticket_type"));
					list.add(4, rs.getInt("expiry_type"));
					list.add(5, rs.getString("expiry_date"));
					list.add(6, rs.getBoolean("display_to_users"));
					list.add(7, rs.getInt("ticket_status"));
					list.add(8, rs.getString("created_by"));
					list.add(9, rs.getString("created_on"));
					list.add(10, rs.getString("update_date"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}


	}//end of ticketDetailsByID


	public int getCount() {
		String query = String.format("select count(id) from ticket_creation_details;");

		Integer result= null;

		try {

			
		    result =  jdbcTemplate.queryForObject(query, Integer.class);
		    return result;


		} catch (EmptyResultDataAccessException e) {
			return 0;
		}

	}




	public List<List<Object>> getTicketDetails() {
		String query = "select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details order by id;";
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("id"));
					list.add(1, rs.getString("name"));
					list.add(2, rs.getInt("ticket_value"));
					list.add(3, rs.getInt("ticket_type"));
					list.add(4, rs.getInt("expiry_type"));
					list.add(5, rs.getString("expiry_date"));
					list.add(6, rs.getBoolean("display_to_users"));
					list.add(7, rs.getInt("ticket_status"));
					list.add(8, rs.getString("created_by"));
					list.add(9, rs.getString("created_on"));
					list.add(10, rs.getString("update_date"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}// end of getTicketDetails

	public List<List<Object>> getTickDetailsAfterCreate(int ticketid){
		String query="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";

		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("id"));
					list.add(1, rs.getString("name"));
					list.add(2, rs.getInt("ticket_value"));
					list.add(3, rs.getInt("ticket_type"));
					list.add(4, rs.getInt("expiry_type"));
					list.add(5, rs.getString("expiry_date"));
					list.add(6, rs.getBoolean("display_to_users"));
					list.add(7, rs.getInt("ticket_status"));
					list.add(8, rs.getString("created_by"));
					list.add(9, rs.getString("created_on"));
					list.add(10, rs.getString("update_date"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}


	}//end of ticketDetailsByID


	public List<List<Object>> getAssignedTicket(int ticketId){
		//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
		String query="select userid,status from ticket_users_map where ticket_id="+ticketId+" order by id desc limit 10";	
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getInt("userid"));
					list.add(1, rs.getInt("status"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}


	}//end of getAssignedTicket


	//Milestone 3
	public List<List<Object>> getUseTicket(int ticketId,int userid){
		//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
			String query="select status from ticket_users_map where ticket_id="+ticketId+" and userid="+userid;	
				try {

					return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
						@Override
						public List<Object> mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List<Object> list = new ArrayList<Object>();
							list.add(0, rs.getInt("status"));
							//list.add(1, rs.getInt("status"));
							return list;
						}
					});
				} catch (EmptyResultDataAccessException e) {
					return null;
				}
				
				
			}
	
	
	public List<List<Object>> getResetTicket(int Tournamentid,int userid){
		//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
			String query="select status from ticket_usage_map where userid="+userid+" and tournamentid="+Tournamentid+" order by id desc limit 1";	
				try {

					return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
						@Override
						public List<Object> mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List<Object> list = new ArrayList<Object>();
							list.add(0, rs.getInt("status"));
							//list.add(1, rs.getInt("status"));
							return list;
						}
					});
				} catch (EmptyResultDataAccessException e) {
					return null;
				}
				
				
			}
	
	public List<List<Object>> getViewUserTicketsDisplayToUser(int userid, String disp){

		//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
			String query="select ts.id,tc.id as ticketid,  tc.name,tc.ticket_type,tc.expiry_date as ticket_exp_date,ts.status, ts.comments,ts.created_on, ts.expiry_date as user_exp_date,tm.tournamentid, max(tm.updatedate) as maxupdate  from ticket_creation_details tc join ticket_users_map ts on ts.ticket_id = tc.id  and ts.userid = "+userid+ " and  tc.display_to_users ="+disp+" left outer join ticket_usage_map tm on ts.id = tm.ticket_user_id  and tm.ticket_id = tc.id group by ts.id,tm.ticket_user_id order by ts.created_on desc";	
				try {

					return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
						@Override
						public List<Object> mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List<Object> list = new ArrayList<Object>();
							list.add(0, rs.getInt("id"));
							list.add(1, rs.getInt("ticketid"));
							list.add(2, rs.getString("name"));
							list.add(3, rs.getInt("ticket_type"));
							list.add(4, rs.getString("ticket_exp_date"));
							list.add(5, rs.getInt("status"));
							list.add(6, rs.getString("comments"));
							list.add(7, rs.getString("created_on"));
							list.add(8, rs.getString("user_exp_date"));
							list.add(9, rs.getInt("tournamentid"));
							list.add(10, rs.getString("maxupdate"));
							
							return list;
						}
					});
				} catch (EmptyResultDataAccessException e) {
					return null;
				}
				
	}
	
	public List<List<Object>> getViewUserTicketsStatus(int userid,int status){


		//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
			String query="select ts.id,tc.id as ticketid,tc.name,tc.ticket_type,tc.expiry_date as ticket_exp_date,ts.status, ts.comments,ts.created_on, ts.expiry_date as user_exp_date,tm.tournamentid, max(tm.updatedate) as maxupdate  from ticket_creation_details tc join ticket_users_map ts on ts.ticket_id = tc.id  and ts.userid = "+userid+ " and  ts.status="+status+" left outer join ticket_usage_map tm on ts.id = tm.ticket_user_id  and tm.ticket_id = tc.id group by ts.id,tm.ticket_user_id order by ts.created_on desc";	
				try {

					return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
						@Override
						public List<Object> mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List<Object> list = new ArrayList<Object>();
							list.add(0, rs.getInt("id"));
							list.add(1, rs.getInt("ticketid"));
							list.add(2, rs.getString("name"));
							list.add(3, rs.getInt("ticket_type"));
							list.add(4, rs.getString("ticket_exp_date"));
							list.add(5, rs.getInt("status"));
							list.add(6, rs.getString("comments"));
							list.add(7, rs.getString("created_on"));
							list.add(8, rs.getString("user_exp_date"));
							list.add(9, rs.getInt("tournamentid"));
							list.add(10, rs.getString("maxupdate"));
							
							return list;
						}
					});
				} catch (EmptyResultDataAccessException e) {
					return null;
				}
				
	
		
	}
	
	
	public List<List<Object>> getViewUserTicketsLimit(int userid,int limit){



		//String query1="select id,name,ticket_value,ticket_type,expiry_type,expiry_date,display_to_users,ticket_status,created_by,created_on,update_date from ticket_creation_details where id="+ticketid+";";
			String query="select ts.id,tc.id as ticketid,  tc.name,tc.ticket_type,tc.expiry_date as ticket_exp_date,ts.status, ts.comments,ts.created_on, ts.expiry_date as user_exp_date,tm.tournamentid, max(tm.updatedate) as maxupdate  from ticket_creation_details tc join ticket_users_map ts on ts.ticket_id = tc.id  and ts.userid ="+userid +" left outer join ticket_usage_map tm on ts.id = tm.ticket_user_id  and tm.ticket_id = tc.id group by ts.id,tm.ticket_user_id order by ts.created_on desc limit "+limit;	
				try {

					return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
						@Override
						public List<Object> mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							List<Object> list = new ArrayList<Object>();
							list.add(0, rs.getInt("id"));
							list.add(1, rs.getInt("ticketid"));
							list.add(2, rs.getString("name"));
							list.add(3, rs.getInt("ticket_type"));
							list.add(4, rs.getString("ticket_exp_date"));
							list.add(5, rs.getInt("status"));
							list.add(6, rs.getString("comments"));
							list.add(7, rs.getString("created_on"));
							list.add(8, rs.getString("user_exp_date"));
							list.add(9, rs.getInt("tournamentid"));
							list.add(10, rs.getString("maxupdate"));
							
							return list;
						}
					});
				} catch (EmptyResultDataAccessException e) {
					return null;
				}
				
	
		
	
		
	}
	
	public List<List<Object>> getInfoForBulkRPCredit(){
		String query="select playerid,corrected_rp_amount from rp_correction_temp where corrected_rp_amount>0 and is_done=0";	
		try {

			return jdbcTemplate.query(query, new RowMapper<List<Object>>() {
				@Override
				public List<Object> mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					List<Object> list = new ArrayList<Object>();
					list.add(0, rs.getLong("playerid"));
					list.add(1, rs.getDouble("corrected_rp_amount"));
					return list;
				}
			});
		} catch (EmptyResultDataAccessException e) {
			return null;
		}


	}
	
}// end of NFSDDAO class
