package com.rummycircle.BonusServiceM3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class Stringtokenizer {
	
	public static List<String> tokenize(final String str, final String delimiter) {
		if (str == null || str.trim().isEmpty() || delimiter == null || delimiter.trim().isEmpty()) {
		return Collections.emptyList();
		}
		StringTokenizer tokenizer = new StringTokenizer(str.trim(), delimiter.trim());
		List<String> list = new ArrayList<>();
		while (tokenizer.hasMoreTokens()) {
		list.add(tokenizer.nextToken().trim());
		}
		return list;
		}
	

}
