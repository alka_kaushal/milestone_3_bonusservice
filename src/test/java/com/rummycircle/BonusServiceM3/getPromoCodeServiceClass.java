package com.rummycircle.BonusServiceM3;

import java.util.List;

public class getPromoCodeServiceClass {
	
	private List<getPromoCodeServiceClass> PromoCodeServiceList;
	private int id;
    private int promotionType;
    private String promotionCode;
    private String promoStartDate;
    private String promoEndDate;
    private int promoUsage;
    private int criteriaId;
    private int bonusType;
    private long bonusAmount;
    private long minDeposit;
    private long percentage;
    private long maxBonusAmount;
    private int chunks;
    private long wager;
    private int bonusExpiryDays;
    private int promoCodeStatus;
    private int createdBy;
    private String creationDate;
    private int lastUpdateBy;
    private String lastUpdateDate;
    private int campaignId;
    private String terms_conditions_link;
    private int channelId;
    private int isDisplayed;
    private int limitedBonusType;
   
    
    public List<getPromoCodeServiceClass> getPromoCodeServiceList() {
		return PromoCodeServiceList;
	}
	public void setPromoCodeServiceList(
			List<getPromoCodeServiceClass> promoCodeServiceList) {
		PromoCodeServiceList = promoCodeServiceList;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getPromoStartDate() {
		return promoStartDate;
	}
	public void setPromoStartDate(String promoStartDate) {
		this.promoStartDate = promoStartDate;
	}
	public String getPromoEndDate() {
		return promoEndDate;
	}
	public void setPromoEndDate(String promoEndDate) {
		this.promoEndDate = promoEndDate;
	}
	public int getPromoUsage() {
		return promoUsage;
	}
	public void setPromoUsage(int promoUsage) {
		this.promoUsage = promoUsage;
	}
	public int getCriteriaId() {
		return criteriaId;
	}
	public void setCriteriaId(int criteriaId) {
		this.criteriaId = criteriaId;
	}
	public int getBonusType() {
		return bonusType;
	}
	public void setBonusType(int bonusType) {
		this.bonusType = bonusType;
	}
	public long getBonusAmount() {
		return bonusAmount;
	}
	public void setBonusAmount(long bonusAmount) {
		this.bonusAmount = bonusAmount;
	}
	public long getMinDeposit() {
		return minDeposit;
	}
	public void setMinDeposit(long minDeposit) {
		this.minDeposit = minDeposit;
	}
	public long getPercentage() {
		return percentage;
	}
	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}
	public long getMaxBonusAmount() {
		return maxBonusAmount;
	}
	public void setMaxBonusAmount(long maxBonusAmount) {
		this.maxBonusAmount = maxBonusAmount;
	}
	public int getChunks() {
		return chunks;
	}
	public void setChunks(int chunks) {
		this.chunks = chunks;
	}
	public long getWager() {
		return wager;
	}
	public void setWager(long wager) {
		this.wager = wager;
	}
	public int getBonusExpiryDays() {
		return bonusExpiryDays;
	}
	public void setBonusExpiryDays(int bonusExpiryDays) {
		this.bonusExpiryDays = bonusExpiryDays;
	}
	public int getPromoCodeStatus() {
		return promoCodeStatus;
	}
	public void setPromoCodeStatus(int promoCodeStatus) {
		this.promoCodeStatus = promoCodeStatus;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public int getLastUpdateBy() {
		return lastUpdateBy;
	}
	public void setLastUpdateBy(int lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public int getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}
	public String getTerms_conditions_link() {
		return terms_conditions_link;
	}
	public void setTerms_conditions_link(String terms_conditions_link) {
		this.terms_conditions_link = terms_conditions_link;
	}
	public int getChannelId() {
		return channelId;
	}
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}
	public int getIsDisplayed() {
		return isDisplayed;
	}
	public void setIsDisplayed(int isDisplayed) {
		this.isDisplayed = isDisplayed;
	}
	public int getLimitedBonusType() {
		return limitedBonusType;
	}
	public void setLimitedBonusType(int limitedBonusType) {
		this.limitedBonusType = limitedBonusType;
	}

		@Override
		public String toString() {
			return "getPromoCodeServiceClass [id=" + id + ", promotionType="
					+ promotionType + ", promotionCode=" + promotionCode
					+ ", promoStartDate=" + promoStartDate + ", promoEndDate="
					+ promoEndDate + ", promoUsage=" + promoUsage + ", criteriaId="
					+ criteriaId + ", bonusType=" + bonusType + ", bonusAmount="
					+ bonusAmount + ", minDeposit=" + minDeposit + ", percentage="
					+ percentage + ", maxBonusAmount=" + maxBonusAmount
					+ ", chunks=" + chunks + ", wager=" + wager
					+ ", bonusExpiryDays=" + bonusExpiryDays + ", promoCodeStatus="
					+ promoCodeStatus + ", createdBy=" + createdBy
					+ ", creationDate=" + creationDate + ", lastUpdateBy="
					+ lastUpdateBy + ", lastUpdateDate=" + lastUpdateDate
					+ ", campaignId=" + campaignId + ", terms_conditions_link="
					+ terms_conditions_link + ", channelId=" + channelId
					+ ", isDisplayed=" + isDisplayed + ", limitedBonusType="
					+ limitedBonusType + "]";
		}
		

}
